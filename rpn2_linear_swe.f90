!
! Solves the linear shallow water equations
!
subroutine rpn2(ixy,maxm,meqn,mwaves,maux,mbc,mx,ql,qr,auxl,auxr,fwave,s,amdq,apdq)

    implicit none

    !input
    integer, intent(in) :: maxm,meqn,maux,mwaves,mbc,mx,ixy

    real(kind=8), intent(in) :: ql(meqn, 1-mbc:maxm+mbc)
    real(kind=8), intent(in) :: qr(meqn, 1-mbc:maxm+mbc)
    real(kind=8), intent(in) :: auxl(maux,1-mbc:maxm+mbc)
    real(kind=8), intent(in) :: auxr(maux,1-mbc:maxm+mbc)

    real(kind=8), intent(in out) :: fwave(meqn, mwaves, 1-mbc:maxm+mbc)
    real(kind=8), intent(in out) :: s(mwaves, 1-mbc:maxm+mbc)
    real(kind=8), intent(in out) :: apdq(meqn,1-mbc:maxm+mbc)
    real(kind=8), intent(in out) :: amdq(meqn,1-mbc:maxm+mbc)

    !local only
    integer :: m,i,mw,maxiter,mu,nv
    real(kind=8) wall(3)
    real(kind=8) fw(3,3)
    real(kind=8) sw(3)

    real(kind=8) :: hR,hL,huR,huL,uR,uL,hvR,hvL,vR,vL,phiR,phiL
    real(kind=8) :: bR,bL,sL,sR,sRoe1,sRoe2,sE1,sE2,uhat,chat
    real(kind=8) :: s1m,s2m
    real(kind=8) :: hstar,hstartest,hstarHLL,sLtest,sRtest
    real(kind=8) :: tw

    logical :: rare1,rare2

    real(kind=8) :: g, drytol

    common /cparam/ g, drytol

    ! Initialize values
    s = 0.d0
    fwave = 0.d0

    ! Set normal direction
    if (ixy == 1) then
        mu=2
        nv=3
    else
        mu=3
        nv=2
    endif

    !loop through Riemann problems at each grid cell
    do i=2-mbc,mx+mbc

        ! Skip completely dry cell
        if (qr(1, i-1) < drytol .and. ql(1, i) < drytol) then
            cycle
        end if

        ! Extract states
        hL = qr(1, i-1)
        hR = ql(1, i)
        huL = qr(mu, i-1)
        huR = ql(mu, i)
        hvL = qr(nv, i-1)
        hvR = ql(nv, i)
        bL = auxr(1, i-1)
        bR = auxl(1, i)

        if (hL < drytol) then
            hL = 0.d0
            huL = 0.d0
            hvL = 0.d0
            uL = 0.d0
            vL = 0.d0
        else
            uL = huL / hL
            vL = hvL / hL
        endif

        if (hR < drytol) then
            hR = 0.d0
            huR = 0.d0
            hvR = 0.d0
            uR = 0.d0
            vR = 0.d0
        else
            uR = huR / hR
            vR = hvR / hR
        endif

        ! Eigen speeds
        sL = -sqrt(g * hL)
        sR =  sqrt(g * hR)

        ! Compute waves
        fwave(1, 1, i) = 0.d0
        fwave(mu, 1, i) = 0.d0
        fwave(nv, 1, i) = 0.d0
        fwave(4, 1, i) = 0.d0

        fwave(1, 2, i) = 0.d0
        fwave(mu, 2, i) = 0.d0
        fwave(nv, 2, i) = 0.d0
        fwave(4, 2, i) = 0.d0

        fwave(1, 3, i) = 0.d0
        fwave(mu, 3, i) = 0.d0
        fwave(nv, 3, i) = 0.d0
        fwave(4, 3, i) = 0.d0

        ! Include advected tracer
        fwave(4,1,i) = 0.d0
        fwave(4,2,i) = 0.d0
        fwave(4,3,i) = 0.d0
        fwave(4,4,i) = 0.5d0 * (uR + uL) * (ql(4,i) - qr(4,i-1))
        s(4, i) = 0.5d0 * (uR + uL)

        endif
    enddo
    !===========================================================================


    !============= compute fluctuations=========================================
    amdq = 0.d0
    apdq = 0.d0
    do i=2-mbc,mx+mbc
        do  mw=1,mwaves
            if (s(mw,i) < 0.d0) then
                amdq(:,i) = amdq(:,i) + fwave(:,mw,i)
            else if (s(mw,i) > 0.d0) then
                apdq(:,i)  = apdq(:,i) + fwave(:,mw,i)
            else
                amdq(:,i) = amdq(:,i) + 0.5d0 * fwave(:,mw,i)
                apdq(:,i) = apdq(:,i) + 0.5d0 * fwave(:,mw,i)
            endif
        enddo
    enddo
end subroutine rpn2
