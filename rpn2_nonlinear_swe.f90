subroutine rpn2(ixy,maxm,meqn,mwaves,maux,mbc,mx,ql,qr,auxl,auxr,fwave,s,amdq,apdq)
!======================================================================
!
! Solves normal Riemann problems for the 2D SHALLOW WATER equations
!     with topography:
!     #        h_t + (hu)_x + (hv)_y = 0                           #
!     #        (hu)_t + (hu^2 + 0.5gh^2)_x + (huv)_y = -ghb_x      #
!     #        (hv)_t + (huv)_x + (hv^2 + 0.5gh^2)_y = -ghb_y      #

! On input, ql contains the state vector at the left edge of each cell
!     qr contains the state vector at the right edge of each cell
!
! This data is along a slice in the x-direction if ixy=1
!     or the y-direction if ixy=2.

!  Note that the i'th Riemann problem has left state qr(i-1,:)
!     and right state ql(i,:)
!  From the basic clawpack routines, this routine is called with
!     ql = qr
!
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                                                           !
!      # This Riemann solver is for the shallow water equations.            !
!                                                                           !
!       It allows the user to easily select a Riemann solver in             !
!       riemannsolvers_geo.f. this routine initializes all the variables    !
!       for the shallow water equations, accounting for wet dry boundary    !
!       dry cells, wave speeds etc.                                         !
!                                                                           !
!           David George, Vancouver WA, Feb. 2009                           !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!       use geoclaw_module, only: g => grav, drytol => dry_tolerance
!       use geoclaw_module, only: earth_radius, deg2rad
!       use amr_module, only: mcapa

    implicit none

    !input
    integer, intent(in) :: maxm,meqn,maux,mwaves,mbc,mx,ixy

    real(kind=8), intent(in) :: ql(meqn, 1-mbc:maxm+mbc)
    real(kind=8), intent(in) :: qr(meqn, 1-mbc:maxm+mbc)
    real(kind=8), intent(in) :: auxl(maux,1-mbc:maxm+mbc)
    real(kind=8), intent(in) :: auxr(maux,1-mbc:maxm+mbc)

    real(kind=8), intent(in out) :: fwave(meqn, mwaves, 1-mbc:maxm+mbc)
    real(kind=8), intent(in out) :: s(mwaves, 1-mbc:maxm+mbc)
    real(kind=8), intent(in out) :: apdq(meqn,1-mbc:maxm+mbc)
    real(kind=8), intent(in out) :: amdq(meqn,1-mbc:maxm+mbc)

    !local only
    integer :: m,i,mw,maxiter,mu,nv
    real(kind=8) wall(3)
    real(kind=8) fw(3,3)
    real(kind=8) sw(3)

    real(kind=8) :: hR,hL,huR,huL,uR,uL,hvR,hvL,vR,vL,phiR,phiL
    real(kind=8) :: bR,bL,sL,sR,sRoe1,sRoe2,sE1,sE2,uhat,chat
    real(kind=8) :: s1m,s2m
    real(kind=8) :: hstar,hstartest,hstarHLL,sLtest,sRtest
    real(kind=8) :: tw

    logical :: rare1,rare2

    real(kind=8) :: g, drytol

    common /cparam/ g, drytol

    ! Initialize values
    s = 0.d0
    fwave = 0.d0

    !loop through Riemann problems at each grid cell
    do i=2-mbc,mx+mbc

    !-----------------------Initializing-----------------------------------
        !inform of a bad riemann problem from the start
        if((qr(1,i-1) < 0.d0).or.(ql(1,i)  <  0.d0)) then
            print *, 'Negative input: hl,hr,i=',qr(1,i-1),ql(1,i),i
        endif

        !set normal direction
        if (ixy == 1) then
            mu=2
            nv=3
        else
            mu=3
            nv=2
        endif

        ! Only continue if region is wet
        if (qr(1,i-1) > drytol .and. ql(1,i) > drytol) then

            !Riemann problem variables
            hL = qr(1,i-1) 
            hR = ql(1,i) 
            huL = qr(mu,i-1) 
            huR = ql(mu,i) 
            bL = auxr(1,i-1)
            bR = auxl(1,i)

            hvL=qr(nv,i-1) 
            hvR=ql(nv,i)

            !check for wet/dry boundary
            if (hR > drytol) then
                uR=huR/hR
                vR=hvR/hR
                phiR = 0.5d0*g*hR**2 + huR**2/hR
            else
                hR = 0.d0
                huR = 0.d0
                hvR = 0.d0
                uR = 0.d0
                vR = 0.d0
                phiR = 0.d0
            endif

            if (hL > drytol) then
                uL=huL/hL
                vL=hvL/hL
                phiL = 0.5d0*g*hL**2 + huL**2/hL
            else
                hL=0.d0
                huL=0.d0
                hvL=0.d0
                uL=0.d0
                vL=0.d0
                phiL = 0.d0
            endif

            wall(1) = 1.d0
            wall(2) = 1.d0
            wall(3) = 1.d0
            if (hR <= drytol) then
                call riemanntype(hL,hL,uL,-uL,hstar,s1m,s2m,    &
                                            rare1,rare2,1,drytol,g)
                hstartest=max(hL,hstar)
                ! Right state should become ghost values that mirror left for 
                ! wall problem
                if (hstartest+bL.lt.bR) then 
                    wall(2)=0.d0
                    wall(3)=0.d0
                    hR=hL
                    huR=-huL
                    bR=bL
                    phiR=phiL
                    uR=-uL
                    vR=vL
                else if (hL+bL < bR) then
                    bR=hL+bL
                endif
            else if (hL <= drytol) then ! right surface is lower than left topo
                call riemanntype(hR,hR,-uR,uR,hstar,s1m,s2m,        &
                                            rare1,rare2,1,drytol,g)
                hstartest=max(hR,hstar)
                !left state should become ghost values that mirror right
                if (hstartest+bR.lt.bL) then  
                    wall(1)=0.d0
                    wall(2)=0.d0
                    hL=hR
                    huL=-huR
                    bL=bR
                    phiL=phiR
                    uL=-uR
                    vL=vR
                else if (hR+bR < bL) then
                    bL=hR+bR
                endif
            endif

            !determine wave speeds
            sL=uL-sqrt(g*hL) ! 1 wave speed of left state
            sR=uR+sqrt(g*hR) ! 2 wave speed of right state

            uhat=(sqrt(g*hL)*uL + sqrt(g*hR)*uR)/(sqrt(g*hR)+sqrt(g*hL)) ! Roe average
            chat=sqrt(g*0.5d0*(hR+hL)) ! Roe average
            sRoe1=uhat-chat ! Roe wave speed 1 wave
            sRoe2=uhat+chat ! Roe wave speed 2 wave

            sE1 = min(sL,sRoe1) ! Eindfeldt speed 1 wave
            sE2 = max(sR,sRoe2) ! Eindfeldt speed 2 wave

            !--------------------end initializing...finally----------
            !solve Riemann problem.

            maxiter = 1

            call riemann_aug_JCP(maxiter,3,3,hL,hR,huL,huR,hvL,hvR,bL,bR,   &
                                 uL,uR,vL,vR,phiL,phiR,sE1,sE2,drytol,g,sw,fw)

    !         call riemann_ssqfwave(maxiter,meqn,mwaves,hL,hR,huL,huR,
    !     &     hvL,hvR,bL,bR,uL,uR,vL,vR,phiL,phiR,sE1,sE2,drytol,g,sw,fw)

    !          call riemann_fwave(meqn,mwaves,hL,hR,huL,huR,hvL,hvR,
    !     &      bL,bR,uL,uR,vL,vR,phiL,phiR,sE1,sE2,drytol,g,sw,fw)

    !       !eliminate ghost fluxes for wall
            do mw=1,3
                sw(mw)=sw(mw)*wall(mw)

                fw(1,mw)=fw(1,mw)*wall(mw) 
                fw(2,mw)=fw(2,mw)*wall(mw)
                fw(3,mw)=fw(3,mw)*wall(mw)
            enddo

            do mw=1,mwaves
                s(mw,i)=sw(mw)
                fwave(1,mw,i)=fw(1,mw)
                fwave(mu,mw,i)=fw(2,mw)
                fwave(nv,mw,i)=fw(3,mw)
                fwave(4,mw,i) = 0.d0
            enddo

            ! Include advected tracer
            fwave(4,1,i) = 0.d0
            fwave(4,2,i) = 0.d0
            fwave(4,3,i) = 0.d0
            fwave(4,4,i) = 0.5d0 * (uR + uL) * (ql(4,i) - qr(4,i-1))
            s(4,i) = 0.5d0 * (uR + uL)

        endif
    enddo
    !===========================================================================


    !============= compute fluctuations=========================================
    amdq = 0.d0
    apdq = 0.d0
    do i=2-mbc,mx+mbc
        do  mw=1,mwaves
            if (s(mw,i) < 0.d0) then
                amdq(:,i) = amdq(:,i) + fwave(:,mw,i)
            else if (s(mw,i) > 0.d0) then
                apdq(:,i)  = apdq(:,i) + fwave(:,mw,i)
            else
                amdq(:,i) = amdq(:,i) + 0.5d0 * fwave(:,mw,i)
                apdq(:,i) = apdq(:,i) + 0.5d0 * fwave(:,mw,i)
            endif
        enddo
    enddo

end subroutine rpn2
