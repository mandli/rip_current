#!/usr/bin/env python
# encoding: utf-8
r"""
Read in subquential output files and average them over a single period

:Authors:
	Kyle Mandli (2011-10-07) Initial version
"""
# ============================================================================
#      Copyright (C) 2011 Kyle Mandli <mandli@amath.washington.edu>
#
#  Distributed under the terms of the Berkeley Software Distribution (BSD) 
#  license
#                     http://www.opensource.org/licenses/
# ============================================================================

import sys
import os

import numpy as np
import matplotlib.pyplot as plt

from pyclaw.solution import Solution
from pyclaw.data import Data

def integrate(t,f):
    dt = t[1:] - t[:-1]
    return 0.5 * np.sum(dt * (f[1:] + f[:-1]))

out_dir = "_output"
DRY_TOL = 1e-6

# Data files
claw_data = Data(os.path.join(out_dir,'claw.data'))
wave_data = Data(os.path.join(out_dir,'wave_tank.data'))

# Calculate period
period = 1.0 / abs(wave_data.frequency[0])
  
# Assume outstyle == 3 and get the output times
assert(claw_data.outstyle == 3)
times = np.array([claw_data.t0 + n*claw_data.dt_initial for n in xrange(claw_data.iout[1]+1)])
# assert(abs(period - times[-1]) < 1e-6)

u = []
v = []
for i in xrange(len(times)):
    solution = Solution(i,path=out_dir)
    
    # Calculate u and v
    h = solution.q[:,:,0]
    hu = solution.q[:,:,1]
    hv = solution.q[:,:,2]
    
    u.append(np.zeros(hu.shape))
    v.append(np.zeros(hv.shape))
    index = np.nonzero((np.abs(h) > DRY_TOL) * (h != np.nan))
    u[i][index[0],index[1]] = hu[index[0],index[1]] / h[index[0],index[1]]
    v[i][index[0],index[1]] = hv[index[0],index[1]] / h[index[0],index[1]]

f = np.empty(len(times))
average = np.empty((u[0].shape[0],v[0].shape[0]))
for i in xrange(100):
    for j in xrange(100):
        for k in xrange(len(times)):
            f[k] = u[k][i,j]**2 + v[k][i,j]**2
        average[i,j] = 1. / period * integrate(times,f)

fig = plt.figure()
ax = fig.add_subplot(211)
plot = ax.pcolor(solution.grid.x.center,solution.grid.y.center,solution.q[:,:,0].T)
fig.colorbar(plot)
ax = fig.add_subplot(212)
plot = ax.pcolor(solution.grid.x.center,solution.grid.y.center,average.T)
fig.colorbar(plot)

plt.show()
