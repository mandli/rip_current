#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt

#def forcing(x,t,A=0.2,k=0.2,phi_x=0.0,omega=-4.0,phi_t=0.0):
#    return A*np.sin(k*(x-phi_x)-omega*(t-phi_t))

def forcing(x,t,A=0.2,wave_length=10.0*np.pi,phi_x=0.0,frequency=-2.0/np.pi,phi_t=0.0):
    return A*np.sin(2.0*np.pi*(1.0/wave_length * (x-phi_x) - frequency * (t-phi_t)))

y = np.linspace(0,30.0,300)
t = np.linspace(0,4*0.5*np.pi,200)

T,Y = np.meshgrid(t,y)

T1 = T-0.05*(30.0-Y)
T1 = (T1>0.0) * T1
T2 = T-0.05*Y
T2 = (T2>0.0) * T2

plt.subplot(2,1,1)
plt.pcolor(T,Y,0.2*np.sin(4.0*T1)+0.2*np.sin(4.0*T2))

y = np.linspace(0,10.0*np.pi,300)
t = np.linspace(0,4*0.5*np.pi,200)

T,Y = np.meshgrid(t,y)

T1 = np.pi*T - np.pi*0.05*(10.0*np.pi - Y)
T2 = np.pi*T - np.pi*0.05*Y
plt.subplot(2,1,2)
#plt.pcolor(T,Y,forcing(Y,T,0.2,4.0*0.05,30.0,-4.0,0.0) +
#               forcing(Y,T,0.2,-4.0*0.05,0.0,-4.0,0.0))
plt.pcolor(T,Y,forcing(Y,T,phi_x=30.0) + forcing(Y,T,phi_x=0.0,wave_length=-10.0*np.pi))
#plt.pcolor(T,Y,0.2*np.sin(4.0*T-0.2*(30.0-Y))+0.2*np.sin(4.0*T-0.2*Y))
plt.show()
