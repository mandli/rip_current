#!/usr/bin/env python

r"""Plot the values from the fort.averges file output by
the wave tank simulations.

:Author:
    Kyle T. Mandli (2011-10-05)
"""

import os
import sys
import shutil

import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

import pyclaw.data as data

def read_average_times_file(path):
    r"""Read in the array of average output times"""
    return np.ndfromtxt(os.path.join(path,'ave_times.txt'))

def read_averages_file(path,n):
    r"""Read in the averages file at path."""

    data = np.ndfromtxt(path)
    if len(data.shape) == 1:
        data.shape = (n[0],n[1],1)
    else:
        data.shape = (n[0],n[1],data.shape[1])

    return data
    
def plot_averages(n,input_path,output_path,limits=None,titles=None):
    r"""Plot the all the averages in the particular output index n.
    
    """
    
    # Get the run parameters
    claw_data = data.Data(os.path.join(input_path,'claw.data'))
    x = np.linspace(claw_data.xlower,claw_data.xupper,claw_data.mx)
    y = np.linspace(claw_data.ylower,claw_data.yupper,claw_data.my)
    X,Y = np.meshgrid(x,y)
    
    # Get the average data
    file_path = os.path.join(input_path,'fort.ave%s' % str(str(n).zfill(4)))
    averages = read_averages_file(file_path,[claw_data.mx,claw_data.my])
    
    # Find number of averages
    num_averages = averages.shape[-1]
    
    # Produce the plots
    if num_averages > 2:
        num_cols = np.ceil(num_averages / 2)
        num_rows = 2
    else:
        num_cols = num_averages
        num_rows = 1
    fig = plt.figure(1)
    for i in xrange(num_averages):
        ax = fig.add_subplot(num_rows,num_cols,i+1)
        #plot = ax.pcolor(X,Y,averages[:,:,i])
        if limits is None:
            limits = [np.min(averages[:,:,i]),np.max(averages[:,:,i])]
        color_norm = mpl.colors.Normalize(limits[0],limits[1],clip=True)
        plot = ax.imshow(averages[:,:,i],
                         extent=[claw_data.xlower,claw_data.xupper,
                                 claw_data.ylower,claw_data.yupper],
                         interpolation='nearest',
                         norm=color_norm)
        ax.set_title(titles[i])
        ax.set_xlabel('x (m)')
        ax.set_ylabel('y (m)')
        fig.colorbar(plot)
        
    # Save figure out to file
    out_file = os.path.join(output_path,'avefig%s.png' % str(str(n).zfill(4)))
    fig.savefig(out_file)
    plt.clf()


def write_html_index(output_path,average_times,title="Averages"):

    html_file = open(os.path.join(output_path,'index.html'),'w')
    
    html_file.write("""<html>\n    <header>\n    <title>%s</title>\n   </header>\n    <body>""" % title)
    html_file.write("""<h1>%s</h1>""" % title)
    html_file.write("""</body>\n</html>""")
    
    html_file.close()

if __name__ == "__main__":
    # Parse command line arguments
    input_path = './_output'
    output_path = './average_plots'
    if len(sys.argv) > 1:
        input_path = sys.argv[1]
        if len(sys.argv) > 2:
            output_path = sys.argv[2]
    
    # Create output directory if not already present
    if os.path.exists(output_path):
        shutil.rmtree(output_path)
    os.makedirs(output_path)

    times = read_average_times_file(input_path)
    for (i,t) in enumerate(times):
        print "Plotting fort.ave%s at t=%s" % (str(i+1).zfill(4),t)
        plot_averages(i+1,input_path,output_path,titles=["Test","<uu> at t=%s" % t])

    
