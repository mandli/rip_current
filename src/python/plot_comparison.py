#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt

from pyclaw.solution import Solution
from pyclaw.plotters import colormaps

# Parameters
frames = [4,6,10]
slice_location = 75
forced_path = "./forced_output"
unforced_path = "./unforced_output"
titles = ["eta","hu","hv"]
figsize = (16,6)
cmap = colormaps.make_colormap({1.0:'r',0.5:'w',0.0:'b'})



# Create plots
for frame in frames:

    # Fetch solutions
    sol = []
    sol.append(Solution(frame,path=forced_path))
    sol.append(Solution(frame,path=unforced_path))
    
    # Create figures
    pcolor_fig = plt.figure(figsize=figsize)
    profile_fig = plt.figure(figsize=figsize)
    
    for (m,field) in enumerate([3,1,2]):
        # Pcolor plots
        ax = pcolor_fig.add_subplot(1,3,m+1)
        plot = ax.pcolor(sol[0].grids[0].dimensions[0].center,
                         sol[0].grids[0].dimensions[1].center,
                         sol[0].q[:,:,field].T - sol[1].q[:,:,field].T,
                         cmap=cmap)
        ax.set_title(''.join((titles[m],', t=%s' % sol[0].t)))
        ax.set_xlabel('x')
        ax.set_ylabel('y')
        ax.set_autoscale_on(True)
        pcolor_fig.colorbar(plot)
        
        ax = profile_fig.add_subplot(1,3,m+1)
        plot = ax.plot(sol[0].grids[0].dimensions[0].center,sol[0].q[:,75,field],'r',
                       sol[1].grids[0].dimensions[0].center,sol[1].q[:,75,field],'k')
        ax.set_title(''.join((titles[m],', t=%s' % sol[0].t)))
        ax.set_xlabel('x')
        ax.set_ylabel(titles[m])
        ax.legend(['forced','unforced'])
    pcolor_fig.savefig('compare_pcolor_%s.png' % (frame))
    profile_fig.savefig('compare_profile_%s.png' % (frame))
