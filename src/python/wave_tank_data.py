#!/usr/bin/env python
#
# Module for wave tank parameters
#
# KTM 9-27-11
#
r"""Wave tank parameter file specification
"""

import numpy as np

import pyclaw.data as data

# Wave Tank Data Format
class WaveTankData(data.Data):
    def __init__(self):

        super(WaveTankData,self).__init__()
        
        # Bathymetry parameters
        self.add_attribute('depth',-1.0)
        self.add_attribute('knee',25.0)
        self.add_attribute('angle',np.arctan(0.1))
        
        # Tracer parameters
        self.add_attribute('tracer',[24.0,25.0])
    
        # Friction source terms
        self.add_attribute('friction',True)
        self.add_attribute('coeff',0.025)
     
        # Paddle parameters
        self.add_attribute('amplitude',[0.2,0.2])
        self.add_attribute('wave_length',[0.5*np.pi,0.5*np.pi])
        self.add_attribute('frequency',[10.0*np.pi,10.0*np.pi])
        self.add_attribute('phi_t',[0.0,0.0])
        self.add_attribute('phi_y',[7.5,7.5])

    def write(self,out_file="./wave_tank.data",datasource='setrun.py'):
        r"""Write out wave tank data file"""

        print "Creating data file %s" % out_file
        out_file = open(out_file,"w")
        
        data.data_write(out_file,self,'depth',"(Depth of deep part of wave tank)")
        data.data_write(out_file,self,'knee',"(Location of beach slope start)")
        data.data_write(out_file,self,'angle',"(Angle of beach slope)")
        data.data_write(out_file,self,None)
        
        data.data_write(out_file,self,'tracer',"(Tracer limits)")
        data.data_write(out_file,self,None)
        
        data.data_write(out_file,self,'friction',"(Friction source term on)")
        data.data_write(out_file,self,'coeff',"(Manning's N coefficient of friction)")
        data.data_write(out_file,self,None)
         
        data.data_write(out_file,self,'amplitude',"(Amplitude of paddle)")
        data.data_write(out_file,self,'wave_length',"(Wave length of paddle)")
        data.data_write(out_file,self,'frequency',"(Frequency of paddle)")
        data.data_write(out_file,self,'phi_t',"(Time offset)")
        data.data_write(out_file,self,'phi_y',"(Space offset)")

        out_file.close()
