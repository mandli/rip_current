module afterstep_module

    implicit none
    
    integer, parameter :: T_UNIT = 58
    integer, parameter :: IO_UNIT = 59
    integer, parameter :: NUM_AVERAGES = 6

    double precision, pointer :: averages(:,:,:)
    double precision, pointer :: f_previous(:,:,:)
    double precision, pointer :: f(:,:,:)

contains
    
    subroutine setup_afterstep(mx,my)

        implicit none

        integer, intent(in) :: mx,my
        
        allocate(averages(NUM_AVERAGES,mx,my))
        allocate(f(NUM_AVERAGES,mx,my))
        call reset_averages()

!         open(T_UNIT,file="ave_times.txt",status='unknown',form='formatted')

    end subroutine setup_afterstep
    
    subroutine teardown_afterstep()

        implicit none

!         close(T_UNIT)

    end subroutine teardown_afterstep

    subroutine reset_averages()

        implicit none

        f = 0.d0
        averages = 0.d0

    end subroutine reset_averages


    subroutine afterstep(maxmx,maxmy,mbc,mx,my,meqn,q,xlower,ylower,dx,dy,t,dt,maux,aux)

        use geoclaw_module, only: drytolerance,pi
        use wave_tank_module, only: period

        implicit none

        ! Input/Output Arguments
        integer, intent(in) :: maxmx,maxmy,mbc,mx,my,meqn,maux
        double precision, intent(in) :: xlower,ylower,dx,dy,t,dt
    
        double precision, intent(inout) :: q(meqn, 1-mbc:maxmx+mbc,1-mbc:maxmy+mbc)
        double precision, intent(inout) :: aux(maux, 1-mbc:maxmx+mbc,1-mbc:maxmy+mbc)
    
        ! Local storage
        integer :: i,j
        double precision :: x,y,zeta
        double precision :: u(1-mbc:mx+mbc,1-mbc:my+mbc)
        double precision :: v(1-mbc:mx+mbc,1-mbc:my+mbc)
        double precision :: eta(1-mbc:mx+mbc,1-mbc:my+mbc),vorticity(1:mx,1:my)
        

        ! Save previous functionals since we need them for the integration
        f_previous => f
        f = 0.d0
        
        ! Calculate velocities
        u = 0.d0
        v = 0.d0
        forall(i=1-mbc:mx+mbc,j=1-mbc:my+mbc,q(i,j,1) > drytolerance)
            u(i,j) = q(2,i,j) / q(1,i,j)
            v(i,j) = q(3,i,j) / q(1,i,j)
            eta(i,j) = q(1,i,j) + aux(1,i,j)
        end forall
        vorticity = (u(1:mx,2:my+1) - 2.d0 * u(1:mx,1:my) + u(1:mx,0:my-1)) / (2.d0 * dy) + &
                    (v(2:mx+1,1:my) - 2.d0 * v(1:mx,1:my) + v(0:mx-1,1:my)) / (2.d0 * dx)
        
        ! Bernoulli Head
        f(2,:,:) = u(1:mx,1:my)**2 + v(1:mx,1:my)**2
        ! Vorticity force
        f(3,:,:) =   v(1:mx,1:my) * vorticity           
        f(4,:,:) = - u(1:mx,1:my) * vorticity
        ! u eta / H and v eta / H 
!         f(:,:,5) = u(1:mx,1:my) * eta / (q(1:mx,1:my,1) + eta(1:mx,1:my))
!         f(:,:,6) = v(1:mx,1:my) * eta / (q(1:mx,1:my,1) + eta(1:mx,1:my))
        f(5,:,:) = u(1:mx,1:my) * eta(1:mx,1:my) / q(1,1:mx,1:my)
        f(6,:,:) = v(1:mx,1:my) * eta(1:mx,1:my) / q(1,1:mx,1:my)
        
        forall(i=1:mx,j=1:my)
            ! Test functional
            f(1,i,j) = sin(2.d0 * pi * (  &
                1.d0 / 10.d0*pi * (xlower+(i-0.5d0)*dx + ylower+(j-0.5d0)*dy) &
              - 1.d0 / period * t))
        end forall        
        
        ! Add functionals to running averages
        averages = averages + dt * (f_previous + f)

    end subroutine afterstep

end module afterstep_module    
