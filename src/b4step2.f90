! ============================================================================
!  This routine keeps track of the grid based integrals that are continually 
!  being kept track of.  Currently the aux array contains the following:
!
!  aux(:,:,1) = Bathymetry
!  aux(:,:,2) = 1/2 <U U>
!  aux(:,:,3) = <v zeta>
!  aux(:,:,4) = <-u zeta>

subroutine b4step2(maxmx,maxmy,mbc,mx,my,meqn,q,xlower,ylower,dx,dy,t,dt,maux,aux)

    implicit none

    ! Input/Output Arguments
    integer, intent(in) :: maxmx,maxmy,mbc,mx,my,meqn,maux
    double precision, intent(in) :: xlower,ylower,dx,dy,t,dt
    
    double precision, intent(inout) :: q(meqn,1-mbc:maxmx+mbc,1-mbc:maxmy+mbc)
    double precision, intent(inout) :: aux(maux,1-mbc:maxmx+mbc,1-mbc:maxmy+mbc)
    
end subroutine b4step2
