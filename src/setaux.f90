subroutine setaux(maxmx,maxmy,mbc,mx,my,xlower,ylower,dx,dy,maux,aux)

    use wave_tank_module

    implicit none

    ! Input/Output arguments
    integer, intent(in) :: maxmx,maxmy,mbc,mx,my,maux
    double precision, intent(in) :: xlower,ylower,dx,dy
    
    double precision, intent(inout) :: aux(maux,1-mbc:maxmx+mbc,1-mbc:maxmy+mbc) 

    ! Local storage
    integer :: i,j
    double precision :: x,y

    do i=1,mx
        x = xlower + (i-0.5d0) * dx
        do j=1,my
            y = ylower + (j-0.5d0) * dy
            
            if (x < knee) then
                aux(1,i,j) = depth
            else
                aux(1,i,j) = tan(angle) * (x-knee) + depth
            endif
        enddo
    enddo
    
    ! Rest of aux variables are running integrals so we just need to zero them
    aux(2:maux,:,:) = 0.d0

end subroutine setaux
