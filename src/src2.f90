subroutine src2(maxmx,maxmy,meqn,mbc,mx,my,xlower,ylower,dx,dy,q,maux,aux,t,dt) 

    use wave_tank_module 
    use geoclaw_module, only: grav


    implicit none

    ! Input/Output Arguments
    integer, intent(in) :: maxmx,maxmy,meqn,mbc,mx,my,maux
    double precision, intent(in) :: xlower,ylower,dx,dy,t,dt

    double precision, intent(inout) :: q(meqn,1-mbc:maxmx+mbc, 1-mbc:maxmy+mbc)
    double precision, intent(inout) :: aux(maux,1-mbc:maxmx+mbc, 1-mbc:maxmy+mbc)

    ! Local storage
    integer :: i,j
    double precision :: h,hu,hv,gamma,dgamma

    ! Algorithm parameters
    double precision, parameter :: tolerance = 1.d-30

    ! Friction source term
    if (friction) then
        do i=1,mx
            do j=1,my
                h = q(1,i,j)
                hu = q(2,i,j)
                hv = q(3,i,j)

                if (h < tolerance) then
                    q(2:3,i,j) = 0.d0
                else
                    gamma = sqrt(hu**2 + hv**2) * (grav*coeff**2) / h**(7.d0/3.d0)
                    dgamma = 1.d0 + dt*gamma
                    q(2:3,i,j) = q(2:3,i,j) / dgamma
                endif
            enddo
        enddo
    endif

end subroutine src2
