! Replacement module for more complex geoclaw module with single grid
! classic clawpack.

module geoclaw_module

    implicit none

    ! Constants
    real(kind=8), parameter :: pi = 4.d0 * atan(1.d0)
    real(kind=8), parameter :: grav = 9.81d0
    real(kind=8), parameter :: earth_radius = 6367.5d3
    real(kind=8), parameter :: deg2rad = pi / 180.d0

    ! Algorithm
    real(kind=8), parameter :: drytolerance = 1d-3
    integer, parameter :: coordinate_system = 1

end module geoclaw_module
