subroutine qinit(maxmx,maxmy,meqn,mbc,mx,my,xlower,ylower,dx,dy,q,maux,aux)

    use wave_tank_module

    implicit none
    
    ! Input/Output Arguments
    integer, intent(in) :: maxmx,maxmy,meqn,mbc,mx,my,maux
    double precision, intent(in) :: xlower,ylower,dx,dy

    double precision, intent(inout) :: q(meqn,1-mbc:maxmx+mbc,1-mbc:maxmy+mbc)
    double precision, intent(inout) :: aux(maux,1-mbc:maxmx+mbc,1-mbc:maxmy+mbc)

    ! Local storage
    integer :: i,j
    double precision :: x,y
    double precision, parameter :: sea_level = 0.d0

    do j=1,my
        y = ylower + (j-0.5d0) * dy
        do i=1,mx
            x = xlower + (i-0.5d0) * dx
            q(1,,j) = max(0.d0,sea_level - aux(1,i,j))
            q(2:3,i,j) = 0.d0
            
            if (tracer(1) <= x .and. x <= tracer(2) .and. q(1,i,j) > 0.d0) then
                q(4,i,j) = 1.d0
            else
                q(4,i,j) = 0.d0
            endif

            ! Add bump test
            !q(i,j,1) = q(i,j,1) + &
            !    1.0d0 * exp(-((x-15.d0)**2 + (y-15.d0)**2)/(2.0d0)**2)

        enddo
    enddo

end subroutine qinit
