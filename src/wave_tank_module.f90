! Wave tank module
!
! This module contains settings and parameters for the wave tank
! simulations.
!
! KTM 9-27-2011
!

module wave_tank_module

    implicit none
    
    ! Bathymetry parameters
    double precision, save :: depth, knee, angle
    
    ! Tracer settings
    double precision, save :: tracer(2)
    
    ! Friction settings      
    logical, save :: friction
    double precision, save :: coeff

    ! Paddle forcing parameters
    double precision, dimension(2), save ::  A,wave_length,frequency,phi_t,phi_y
    
    ! Convenience values
    double precision, save :: period

contains

    subroutine set_wave_tank_parameters(file_path)

        implicit none
    
        ! Arguments
        character(len=*), optional, intent(in) :: file_path
    
        ! Local
        integer :: ios, num_output_times
        character(len=120) :: path
    
        if (present(file_path)) then
            path = file_path
        else
            path = './wave_tank.data'
        endif
    
        open(unit=13, file=path, iostat=ios, status="old", action="read")
        if ( ios /= 0 ) then
            print *, "Error opening file ", path
            stop
        endif
    
        read(13,"(1d16.8)") depth
        read(13,"(1d16.8)") knee
        read(13,"(1d16.8)") angle
        read(13,*)
        read(13,*) tracer
        read(13,*)
        read(13,*) friction
        read(13,"(1d16.8)") coeff
        read(13,*)
        read(13,*) A
        read(13,*) wave_length
        read(13,*) frequency   
        read(13,*) phi_t    
        read(13,*) phi_y
    
        ! Calculate period in time
        period = 1.d0 / (sum(abs(frequency)) / size(frequency,1))

        close(13)

    end subroutine set_wave_tank_parameters

end module wave_tank_module
