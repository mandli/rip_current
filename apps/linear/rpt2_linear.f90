subroutine rpt2(ixy,maxm,meqn,mwaves,mbc,mx,ql,qr,aux1,aux2,aux3,ilr,asdq,bmasdq,bpasdq)

    use geoclaw_module

    implicit none

    ! Input
    integer, intent(in) :: ixy,maxm,meqn,mwaves,mbc,mx,ilr

    double precision, intent(in) :: ql(1-mbc:maxm+mbc, meqn)
    double precision, intent(in) :: qr(1-mbc:maxm+mbc, meqn)
    double precision, intent(in) :: asdq(1-mbc:maxm+mbc, meqn)
    double precision, intent(inout) :: bmasdq(1-mbc:maxm+mbc, meqn)
    double precision, intent(inout) :: bpasdq(1-mbc:maxm+mbc, meqn)
    double precision, intent(in) :: aux1(1-mbc:maxm+mbc,*)
    double precision, intent(in) :: aux2(1-mbc:maxm+mbc,*)
    double precision, intent(in) :: aux3(1-mbc:maxm+mbc,*)

    ! Locals
    integer :: i,m,mw,norm_index,tran_index
    double precision :: g,drytol
    double precision :: eta_l,eta_r,h_hat_l,h_hat_r,nu_l,nu_r,h_l,h_r,v_l,v_r
    double precision :: h,v
    double precision :: s(4),R(4,4),beta(4)
    logical :: dry_state(2)
    
    ! Convenience variables
    g = grav
    drytol = drytolerance

    if (ixy == 1) then
        norm_index = 2
        tran_index = 3
    else
        norm_index = 3
        tran_index = 2
    endif
    
    bmasdq = 0.d0
    bpasdq = 0.d0

    do i=2-mbc,mx+mbc
        ! Extract states
        eta_l = qr(i-1,1)
        h_hat_l = aux2(i-1,2)
        h_l = eta_l + h_hat_l
        nu_l = qr(i-1,tran_index)
        
        eta_r = ql(i,1)
        h_hat_r = aux2(i,2)
        h_r = eta_r + h_hat_r
        nu_r = ql(i,tran_index)

        dry_state = .false.
        if (h_l < drytol) then
            dry_state(1) = .true.
            v_l = 0.d0
        else
            v_l = nu_l / h_hat_l
        endif
        if (h_r < drytol) then
            dry_state(2) = .true.
            v_r = 0.d0
        else
            v_r = nu_r / h_hat_r
        endif
        
        if (all(dry_state)) then
            cycle
        endif

        if (ilr == 1) then
            h = h_hat_l
            v = v_l
        else
            h = h_hat_r
            v = v_r
        endif
        
        ! Calculate eigenspace
        s = [-sqrt(g*h),0.d0,v,sqrt(g*h)]
        R = 0.d0
        R(1,1) = 1.d0
        R(1,4) = 1.d0
        R(tran_index,1) = s(1)
        R(tran_index,4) = s(4)
        R(norm_index,2) = 1.d0
        R(4,3) = v

!         print "(4d16.8)",(R(1,mw),mw=1,4)
!         print "(4d16.8)",(R(2,mw),mw=1,4)
!         print "(4d16.8)",(R(3,mw),mw=1,4)
!         print "(4d16.8)",(R(4,mw),mw=1,4)
!         stop

        ! Splitting based on solving R*beta = asdq
        beta(1) = 0.5d0 * (asdq(i,1) - asdq(i,tran_index) / s(4))
        beta(2) = asdq(i,norm_index)
        if (abs(v) > 1d-3) then
            beta(3) = asdq(i,4) / v
        else
            beta(3) = 0.d0
        endif
        beta(4) = 0.5d0 * (asdq(i,1) + asdq(i,tran_index) / s(4))

        ! For each wave, check to see if the eigenspeed is up or down
        do mw=1,4
            if (s(mw) > 0.d0) then
                bpasdq(i,:) = bpasdq(i,:) + R(:,mw) * beta(mw)
            else
                bmasdq(i,:) = bmasdq(i,:) + R(:,mw) * beta(mw)
            endif
        enddo
    enddo

end subroutine rpt2