subroutine rpn2(ixy,maxm,meqn,mwaves,mbc,mx,ql,qr,auxl,auxr,fwave,s,amdq,apdq)
!======================================================================
!
! Solves normal Riemann problems for the linearized 2D SHALLOW WATER equations
!     with topography and an advected tracer:                                       
!              eta_t + (mu)_x + (nu)_y = 0                     
!              (mu)_t + g H eta_x = 0
!              (nu)_t + g H eta_y = 0
!              phi_t + u phi_x + v phi_y = 0
!
! On input, ql contains the state vector at the left edge of each cell
!     qr contains the state vector at the right edge of each cell
!
! This data is along a slice in the x-direction if ixy=1
!     or the y-direction if ixy=2.
!
!  Note that the i'th Riemann problem has left state qr(i-1,:)
!     and right state ql(i,:)
!  From the basic clawpack routines, this routine is called with
!     ql = qr
!

    use geoclaw_module

    implicit none

    ! Input
    integer, intent(in) :: maxm,meqn,mwaves,mbc,mx,ixy

    double precision, intent(inout) :: fwave(1-mbc:maxm+mbc, meqn, mwaves)
    double precision, intent(inout) :: s(1-mbc:maxm+mbc, mwaves)
    double precision, intent(inout) :: ql(1-mbc:maxm+mbc, meqn)
    double precision, intent(inout) :: qr(1-mbc:maxm+mbc, meqn)
    double precision, intent(inout) :: apdq(1-mbc:maxm+mbc, meqn)
    double precision, intent(inout) :: amdq(1-mbc:maxm+mbc, meqn)
    double precision, intent(inout) :: auxl(1-mbc:maxm+mbc, *)
    double precision, intent(inout) :: auxr(1-mbc:maxm+mbc, *)
    
    ! Locals
    integer :: mw,i,norm_index,tran_index
    
    ! Notation
    !  eta = perturbation from sea level
    !  h = eta + h_hat, actual depth of fluid
    !  h_hat = initial depth of fluid
    !  mu,nu = h_hat * u,v, perturbation of momentum
    !  u,v = perturbation of velocity
    !  b = bathymetry
    double precision :: eta_r,eta_l,h_r,h_l,h_hat_r,h_hat_l
    double precision :: drytol,g  
    double precision :: mu_r,mu_l,nu_r,nu_l,u_l,v_l,u_r,v_r
    
    double precision :: h_ave,u_ave,delta(4),beta(4)
    
    logical :: dry_state(2)

    g = grav
    drytol = drytolerance

    ! ========================================================================
    ! Initialize some parameters and output arrays
    if (ixy == 1) then
        norm_index = 2
        tran_index = 3
    else
        norm_index = 3
        tran_index = 2
    endif
    
    fwave = 0.d0
    s = 0.d0
    amdq = 0.d0
    apdq = 0.d0

    ! Loop through Riemann problems at each grid cell
    do i=2-mbc,mx+mbc
        ! =====================================================================
        ! Extract and check states
        dry_state = .false.
        eta_l = qr(i-1,1)
        h_hat_l = auxr(i-1,2)
        h_l = eta_l + h_hat_l
        mu_l = qr(i-1,norm_index)
        nu_l = qr(i-1,tran_index)
        
        eta_r = ql(i,1)
        h_hat_r = auxl(i,2)
        h_r = eta_r + h_hat_r
        mu_r = ql(i,norm_index)
        nu_l = ql(i,tran_index)
        
!         print *,eta_l,eta_r
!         print *,h_hat_l,h_hat_r
!         print *,h_l,h_r
!         print *,mu_l,mu_r
!         print *,nu_l,nu_r
        
        if (h_l < drytol) then
            dry_state(1) = .true.
            u_l = 0.d0
            v_l = 0.d0
            
            ! Check for bad Riemann problem
            if (h_l < 0.d0) then
                print *,"Negative input: h_l,i=",h_l,i
                stop
            endif
        else
            u_l = mu_l / h_hat_l
            v_l = nu_l / h_hat_l
        endif
        
        if (h_r < drytol) then
            dry_state(2) = .true.
            u_r = 0.d0
            v_r = 0.d0
            
            ! Check for bad Riemann problem
            if (h_r < 0.d0) then
                print *,"Negative input: h_r,i=",h_r,i
                stop
            endif
        else
            u_r = mu_r / h_hat_r
            v_r = nu_r / h_hat_r
        endif
            
        ! Skip this cell if both sides are dry
        if (all(dry_state)) then
            cycle
        endif
            
        ! ====================================================================
        ! Calculate waves and speeds
        h_ave = 0.5d0 * (h_hat_l + h_hat_r)
        u_ave = 0.5d0 * (u_l + u_r)
        
        s(i,1) = -sqrt(g*h_ave)
        s(i,2) = 0.d0
        s(i,3) = u_ave
        s(i,4) = sqrt(g*h_ave)
        
        delta(1) = mu_r - mu_l
        delta(norm_index) = g*h_ave*(eta_r - eta_l)
        
        beta(1) = 0.5d0 * (delta(1) - delta(norm_index) / sqrt(g*h_ave))
        beta(4) = 0.5d0 * (delta(1) + delta(norm_index) / sqrt(g*h_ave))
        
        ! Left gravity wave
        fwave(i,1,1) = beta(1)
        fwave(i,norm_index,1) = -sqrt(g*h_ave) * beta(1)
        
        ! Transverse wave, no fluctuations in this wave
        
        ! Tracer wave
        fwave(i,4,3) = u_ave * (ql(i,4) - qr(i-1,4))
        
        ! Right gravity wave
        fwave(i,1,4) = beta(4)
        fwave(i,norm_index,4) = sqrt(g*h_ave) * beta(4)
    enddo

    ! Compute fluctuations
    do i=1-mbc,mx+mbc
        do mw=1,mwaves
            if (s(i,mw) < 0.d0) then
                amdq(i,:) = amdq(i,:) + fwave(i,:,mw)
            else
                apdq(i,:) = apdq(i,:) + fwave(i,:,mw)
            endif
        enddo
    enddo

end subroutine rpn2









