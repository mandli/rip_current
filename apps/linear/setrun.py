""" 
Module to set up run time parameters for Clawpack.

The values set in the function setrun are then written out to data files
that will be read in by the Fortran code.
    
""" 

import os
import numpy as np

from pyclaw import data 

import wave_tank_data

num_steps_per_period = 35

#------------------------------
def setrun(claw_pkg='classic'):
#------------------------------
    
    """ 
    Define the parameters used for running Clawpack.

    INPUT:
        claw_pkg expected to be "classic" for this setrun.

    OUTPUT:
        rundata - object of class ClawRunData 
    
    """ 
    
    assert claw_pkg.lower() == 'classic',  "Expected claw_pkg = 'classic'"

    ndim = 2
    rundata = data.ClawRunData(claw_pkg, ndim)

    #------------------------------------------------------------------
    # Standard Clawpack parameters to be written to claw.data:
    #------------------------------------------------------------------

    clawdata = rundata.clawdata  # initialized when rundata instantiated

    # ---------------
    # Spatial domain:
    # ---------------

    # Number of space dimensions:
    clawdata.ndim = ndim
    
    # Lower and upper edge of computational domain:
    clawdata.xlower = 0.0
    clawdata.xupper = 30.0
    
    clawdata.ylower = 0.0
    clawdata.yupper = 10.0 * np.pi

    # Number of grid cells:
    clawdata.mx = 150
    clawdata.my = 150
        

    # ---------------
    # Size of system:
    # ---------------

    # Number of equations in the system:
    clawdata.meqn = 4

    # Number of auxiliary variables in the aux array (initialized in setaux)
    clawdata.maux = 2
    
    # Index of aux array corresponding to capacity function, if there is one:
    clawdata.mcapa = 0
    
    
    
    # -------------
    # Initial time:
    # -------------

    clawdata.t0 = 0.0
    
    
    # -------------
    # Output times:
    #--------------

    # Specify at what times the results should be written to fort.q files.
    # Note that the time integration stops after the final output time.
    # The solution at initial time t0 is always written in addition.

    clawdata.outstyle = 4

    if clawdata.outstyle==1:
        # Output nout frames at equally spaced times up to tfinal:
        clawdata.nout = 40
        clawdata.tfinal = 800.0

    elif clawdata.outstyle == 2:
        # Specify a list of output times.
        clawdata.tout =  [60.0 + 20.0 * float(i) for i in xrange(0,5)]
        clawdata.nout = len(clawdata.tout)

    elif clawdata.outstyle == 3:
        # Output every iout timesteps with a total of ntot time steps:
        periods = 1
        iout = 1
        ntot = num_steps_per_period*periods
        clawdata.iout = [iout, ntot]
    elif clawdata.outstyle == 4:
        # Output every period
        # tfinal is the time at which the solution is gauranteed to have
        # reached but will not be the exact ending time due to the period
        # output constraint
        tfinal = 800
        clawdata.output_time_interval =  np.pi / 2.0
        clawdata.nout = int(tfinal / clawdata.output_time_interval) + 1
        clawdata.tfinal = clawdata.output_time_interval * clawdata.nout


    # ---------------------------------------------------
    # Verbosity of messages to screen during integration:  
    # ---------------------------------------------------

    # The current t, dt, and cfl will be printed every time step
    # at AMR levels <= verbosity.  Set verbosity = 0 for no printing.
    #   (E.g. verbosity == 2 means print only on levels 1 and 2.)
    clawdata.verbosity = 1
    
    

    # --------------
    # Time stepping:
    # --------------

    # if dt_variable==1: variable time steps used based on cfl_desired,
    # if dt_variable==0: fixed time steps dt = dt_initial will always be used.
    clawdata.dt_variable = 1
    
    # Initial time step for variable dt.  
    # If dt_variable==0 then dt=dt_initial for all steps:
    #if clawdata.outstyle < 3:
    clawdata.dt_initial = 0.016
    #else:
        #clawdata.dt_variable = 0
    #    clawdata.dt_initial = np.pi / (2.0 * num_steps_per_period)

    # Max time step to be allowed if variable dt used:
    clawdata.dt_max = 1e+99
    
    # Desired Courant number if variable dt used, and max to allow without 
    # retaking step with a smaller dt:
    # clawdata.cfl_desired = 0.4
    # clawdata.cfl_max = 0.5
    clawdata.cfl_desired = 0.9
    clawdata.cfl_max = 1.0
    
    # Maximum number of time steps to allow between output times:
    clawdata.max_steps = 5000

    
    

    # ------------------
    # Method to be used:
    # ------------------

    # Order of accuracy:  1 => Godunov,  2 => Lax-Wendroff plus limiters
    clawdata.order = 2
    
    # Transverse order for 2d or 3d (not used in 1d):
    clawdata.order_trans = -2
    
    # Number of waves in the Riemann solution:
    clawdata.mwaves = 4
    
    # List of limiters to use for each wave family:  
    # Required:  len(mthlim) == mwaves
    clawdata.mthlim = [3,3,3,3]
    
    # Source terms splitting:
    #   src_split == 0  => no source term (src routine never called)
    #   src_split == 1  => Godunov (1st order) splitting used, 
    #   src_split == 2  => Strang (2nd order) splitting used,  not recommended.
    clawdata.src_split = 1
    
    
    # --------------------
    # Boundary conditions:
    # --------------------

    # Number of ghost cells (usually 2)
    clawdata.mbc = 2
    
    # Choice of BCs at xlower and xupper:
    #   0 => user specified (must modify bcN.f to use this option)
    #   1 => extrapolation (non-reflecting outflow)
    #   2 => periodic (must specify this at both boundaries)
    #   3 => solid wall for systems where q(2) is normal velocity
    
    clawdata.mthbc_xlower = 0
    clawdata.mthbc_xupper = 3
    
    clawdata.mthbc_ylower = 2
    clawdata.mthbc_yupper = 2
    
    return rundata
    # end of function setrun
    # ----------------------

def set_wave_tank_data(clawdata):
    data = wave_tank_data.WaveTankData()

    # Bathymetry parameters
    data.depth = -1.0
    data.knee = 15.0
    # data.angle = np.arctan(1.0/10.0)
    data.angle = 0.0
    
    # Tracer location
    data.tracer = [22.0,40.0]

    # Friction
    data.friction = True
    data.coeff = 0.025

    # Paddle forcing parameters
    data.amplitude = [0.2,0.2]
    data.wave_length = [10.0*np.pi,-10.0*np.pi]
    data.frequency = [-2./np.pi,-2./np.pi]
    data.phi_t = [0.0,0.0]
    data.phi_y = [30.0,0.0]
    
    # data.amplitude = [0.2,0.0]
    # data.wave_length = [1e99,1e99]
    # data.frequency = [-2./np.pi,-2./np.pi]
    # data.phi_t = [0.0,0.0]
    # data.phi_y = [0.0,0.0]
         
    return data

if __name__ == '__main__':
    # Set up run-time parameters and write all data files.
    import sys
    if len(sys.argv) == 2:
        rundata = setrun(sys.argv[1])
    else:
        rundata = setrun()
    wave_tank_data = set_wave_tank_data(rundata.clawdata)

    rundata.write()
    wave_tank_data.write()
    
