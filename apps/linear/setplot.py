
""" 
Set up the plot figures, axes, and items to be done for each frame.

This module is imported by the plotting routines and then the
function setplot is called to set the plot parameters.
    
"""

import os

#--------------------------
def setplot(plotdata):
#--------------------------
    
    """ 
    Specify what is to be plotted at each frame.
    Input:  plotdata, an instance of pyclaw.plotters.data.ClawPlotData.
    Output: a modified version of plotdata.
    
    """ 
    
    import numpy as np

    import matplotlib.pyplot as plt
    from pyclaw.plotters import colormaps, geoplot
    from pyclaw.data import Data

    clawdata = Data(os.path.join(plotdata.outdir,'claw.data'))
    wavedata = Data(os.path.join(plotdata.outdir,'wave_tank.data'))

    plotdata.clearfigures()  # clear any old figures,axes,items data
    plotdata.clear_frames = False
    plotdata.clear_figs = True
    
    plotdata.save_frames = False
    
    # ========================================================================
    #  Generic Helper Functions
    # ========================================================================
    def pcolor_afteraxes(cd):
        pass
        
    def contour_afteraxes(cd):
        pass
        
    def bathy_ref_lines(cd):
        pass

    # ========================================================================
    #  Water Helper Functions
    # ========================================================================
    def b(cd):
        return cd.q[:,:,3] - cd.q[:,:,0]
        
    def h_hat(cd):
        return cd.q[:,:,3]
        
    def extract_eta(h,eta,DRY_TOL=10**-3):
        index = np.nonzero((np.abs(h) < DRY_TOL) + (h == np.nan))
        eta[index[0],index[1]] = np.nan
        return eta
    
    def eta(cd):
        return extract_eta(cd.q[:,:,0],cd.q[:,:,3])

    def extract_velocity(h,hu,DRY_TOL=10**-8):
        # u = np.ones(hu.shape) * np.nan
        u = np.zeros(hu.shape)
        index = np.nonzero((np.abs(h) > DRY_TOL) * (h != np.nan))
        u[index[0],index[1]] = hu[index[0],index[1]] / h[index[0],index[1]]
        return u
        
    def water_u(cd):
        return extract_velocity(cd.q[:,:,0],cd.q[:,:,1])
        
    def water_v(cd):
        return extract_velocity(cd.q[:,:,0],cd.q[:,:,2])
        
    def water_speed(cd):
        u = water_u(cd)
        v = water_v(cd)
        return np.sqrt(u**2 + v**2)
        
    def water_quiver(cd):
        u = water_u(cd)
        v = water_v(cd)
            
        plt.hold(True)
        Q = plt.quiver(current_data.x[::2,::2],current_data.y[::2,::2],
                        u[::2,::2],v[::2,::2])
        max_speed = np.max(np.sqrt(u**2+v**2))
        label = r"%s m/s" % str(np.ceil(0.5*max_speed))
        plt.quiverkey(Q,0.15,0.95,0.5*max_speed,label,labelpos='W')
        plt.hold(False)
        
    def vorticity(cd):
        r"""Calculate vorticity of velocity field
        
        Using matrix operations, calculate u_y + v_x, note that second order,
        centered differences are used for interior points and forward and
        backward differences for the boundary values.
        
        returns numpy.ndarray of shape of u and v
        """
        u = water_u(cd)
        v = water_v(cd)
        dx = cd.dx
        dy = cd.dy
        
        u_y = np.zeros(u.shape)
        u_y[:,0] = (-3.0*u[:,0] + 4.0 * u[:,1] - u[:,2]) / (2.0 * dy)
        u_y[:,-1] = (u[:,-3] - 4.0 * u[:,-2] + 3.0 * u[:,-1]) / (2.0 * dy)
        u_y[:,1:-1] = (u[:,2:] - u[:,0:-2]) / (2.0 * dy)

        v_x = np.zeros(v.shape)
        v_x[0,:] = (-3.0*v[0,:] + 4.0 * v[1,:] - v[2,:]) / (2.0 * dx)
        v_x[-1,:] = (u[-3,:] - 4.0 * u[-2,:] + 3.0 * u[-1,:]) / (2.0 * dx)
        v_x[1:-1,:] = (v[2:,:] - v[0:-2,:]) / (2.0 * dx)
        
        return v_x - u_y

    # ========================================================================
    #  Profile functions
    # ========================================================================
    class PlotProfile(object):
    
        def __init__(self,slice_value = 0.0,direction='x'):
            self.slice_value = slice_value
            self.direction = direction

        def slice_index(self,cd):
            epsilon = 1e-6
            
            if self.direction == 'x':
                dim = cd.grid.y
            else:
                dim = cd.grid.x

            if self.slice_value <= dim.lower+epsilon:
                return 0
            elif self.slice_value >= dim.upper-epsilon:
                return -1
            elif dim.lower <= self.slice_value <= dim.upper:
                return int((self.slice_value - dim.lower) / dim.d - 0.5)
            else:
                return None
    
        def bathy_profile(self,current_data):
            index = self.slice_index(current_data)
            if index is not None:
                if self.direction == 'x':
                    return current_data.x[:,index], b(current_data)[:,index]
                else:
                    return current_data.y[index,:], b(current_data)[index,:]
            else:
                return None, None
        
        def surface_profile(self,current_data):
            index = self.slice_index(current_data)
            if index is not None:
                if self.direction == 'x':
                    return current_data.x[:,index], eta(current_data)[:,index]
                else:
                    return current_data.y[index,:], eta(current_data)[index,:] 
            else:
                return None, None

        def x_velocity_profile(self,current_data):
            index = self.slice_index(current_data)
            if index is not None:
                if self.direction == 'x':
                    return current_data.x[:,index], water_u(current_data)[:,index]
                else:
                    return current_data.y[index,:], water_u(current_data)[index,:]
            else:
                return None, None
         
        def y_velocity_profile(self,current_data):
            index = self.slice_index(current_data)
            if index is not None:
                if self.direction == 'x':
                    return current_data.x[:,index], water_v(current_data)[:,index]
                else:
                    return current_data.y[index,:], water_v(current_data)[index,:]
            else:
                return None, None
             
    # ========================================================================
    #  Plot items
    # ========================================================================
    def add_surface_elevation(plotaxes,bounds=None,plot_type="pcolor"):
        if plot_type == 'pcolor' or plot_type == 'imshow':            
            plotitem = plotaxes.new_plotitem(plot_type='2d_imshow')
            # plotitem.plotvar = eta
            plotitem.plot_var = geoplot.surface
            plotitem.imshow_cmap = colormaps.make_colormap({1.0:'r',0.5:'w',0.0:'b'})
            if bounds is not None:
                plotitem.imshow_cmin = bounds[0]
                plotitem.imshow_cmax = bounds[1]
            plotitem.add_colorbar = True
            plotitem.amr_gridlines_show = [0,0,0]
            plotitem.amr_gridedges_show = [1,1,1]
        elif plot_type == 'contour':            
            plotitem = plotaxes.new_plotitem(plot_type='2d_contour')
            plotitem.plot_var = geoplot.surface
            if bounds is None:
                plotitem.contour_levels = [-2.5,-1.5,-0.5,0.5,1.5,2.5]
            # plotitem.contour_nlevels = 21
            # plotitem.contour_min = -2.0
            # plotitem.contour_max = 2.0
            # plotitem.kwargs = {''}
            plotitem.amr_contour_show = [1,1,1]
            plotitem.amr_gridlines_show = [0,0,0]
            plotitem.amr_gridedges_show = [1,1,1]
            plotitem.amr_contour_colors = 'k'
            # plotitem.amr_contour_colors = ['r','k','b']  # color on each level
            # plotitem.amr_grid_bgcolor = ['#ffeeee', '#eeeeff', '#eeffee']
        
    def add_speed(plotaxes,bounds=None,plot_type="pcolor"):
        if plot_type == 'pcolor' or plot_type == 'imshow':
            plotitem = plotaxes.new_plotitem(plot_type='2d_imshow')
            plotitem.plot_var = water_speed
            # plotitem.plot_var = 1
            plotitem.imshow_cmap = plt.get_cmap('PuBu')
            if bounds is not None:
                plotitem.imshow_cmin = bounds[0]
                plotitem.imshow_cmax = bounds[1]
            plotitem.add_colorbar = True
            plotitem.amr_gridlines_show = [0,0,0]
            plotitem.amr_gridedges_show = [1]
        elif plot_type == 'quiver':
            plotitem = plotaxes.new_plotitem(plot_type='2d_quiver')
            plotitem.quiver_var_x = water_u
            plotitem.quiver_var_y = water_v
            plotitem.amr_quiver_show = [4,10,10]
            plotitem.amr_show_key = [True,True,False]
            plotitem.key_units = 'm/s'
            
        elif plot_type == 'contour':
            plotitem = plotaxes.new_plotitem(plot_type='2d_contour')
            plotitem.plot_var = water_speed
            plotitem.kwargs = {'linewidths':1}
            # plotitem.contour_levels = [1.0,2.0,3.0,4.0,5.0,6.0]
            plotitem.contour_levels = [0.5,1.5,3,4.5,6.0]
            plotitem.amr_contour_show = [1,1,1]
            plotitem.amr_gridlines_show = [0,0,0]
            plotitem.amr_gridedges_show = [1,1,1]
            plotitem.amr_contour_colors = 'k'
            # plotitem.amr_contour_colors = ['r','k','b']  # color on each level
            # plotitem.amr_grid_bgcolor = ['#ffeeee', '#eeeeff', '#eeffee']

    def add_x_velocity(plotaxes,plot_type='pcolor',bounds=None):
        if plot_type == 'pcolor' or plot_type == 'imshow':
            plotitem = plotaxes.new_plotitem(plot_type='2d_imshow')
            plotitem.plot_var = water_u
            if bounds is not None:
                plotitem.imshow_cmin = bounds[0]
                plotitem.imshow_cmax = bounds[1]
            plotitem.add_colorbar = True
            plotitem.imshow_cmap = plt.get_cmap('PiYG')
            plotitem.amr_gridlines_show = [0,0,0]
            plotitem.amr_gridedges_show = [1]      

    def add_y_velocity(plotaxes,plot_type='pcolor',bounds=None):
        if plot_type == 'pcolor' or plot_type == 'imshow':
            plotitem = plotaxes.new_plotitem(plot_type='2d_imshow')
            plotitem.plot_var = water_v
            if bounds is not None:
                plotitem.imshow_cmin = bounds[0]
                plotitem.imshow_cmax = bounds[1]
            plotitem.imshow_cmap = plt.get_cmap('PiYG')
            plotitem.add_colorbar = True
            plotitem.amr_gridlines_show = [0,0,0]
            plotitem.amr_gridedges_show = [1]

    def add_tracer(plotaxes,bounds=None,plot_type="pcolor"):
        if plot_type == 'pcolor' or plot_type == 'imshow':
            plotitem = plotaxes.new_plotitem(plot_type='2d_imshow')
            plotitem.plot_var = 4
            plotitem.imshow_cmap = plt.get_cmap('PuBu')
            if bounds is not None:
                plotitem.imshow_cmin = bounds[0]
                plotitem.imshow_cmax = bounds[1]
            plotitem.add_colorbar = True
            plotitem.amr_gridlines_show = [0,0,0]
            plotitem.amr_gridedges_show = [1]
        
    def add_vorticity(plotaxes,bounds=None,plot_type="pcolor"):
        if plot_type == 'pcolor' or plot_type == 'imshow':            
            plotitem = plotaxes.new_plotitem(plot_type='2d_imshow')
            plotitem.plot_var = vorticity
            plotitem.imshow_cmap = plt.get_cmap('PRGn')
            if bounds is not None:
                plotitem.imshow_cmin = bounds[0]
                plotitem.imshow_cmax = bounds[1]
            plotitem.add_colorbar = True
            plotitem.amr_gridlines_show = [0,0,0]
            plotitem.amr_gridedges_show = [1]
    
    def add_average(plotaxes,average,bounds=None,plot_type='pcolor'):
        if plot_type == 'pcolor' or plot_type == 'imshow':
            plotitem = plotaxes.new_plotitem(plot_type='2d_imshow')
            plotitem.plot_var = average + 4
            plotitem.imshow_cmap = colormaps.make_colormap({1.0:'r',0.5:'w',0.0:'b'})
            if bounds is not None:
                plotitem.imshow_cmin = bounds[0]
                plotitem.imshow_cmax = bounds[1]
            plotitem.add_colorbar = True
            plotitem.amr_gridlines_show = [0,0,0]
            plotitem.amr_gridedges_show = [1]
    
    def add_land(plotaxes,plot_type="pcolor"):
        if plot_type == 'pcolor':
            plotitem = plotaxes.new_plotitem(plot_type='2d_pcolor')
            plotitem.show = True
            plotitem.plot_var = geoplot.land
            plotitem.pcolor_cmap = geoplot.land_colors
            plotitem.pcolor_cmin = -3.0
            plotitem.pcolor_cmax = 1.0
            plotitem.add_colorbar = False
            plotitem.amr_gridlines_show = [0,0,0]
            plotitem.amr_gridedges_show = [1,1,1]
        elif plot_type == 'contour':            
            plotitem = plotaxes.new_plotitem(plot_type='2d_contour')
            plotitem.plot_var = geoplot.land
            plotitem.contour_nlevels = 40
            plotitem.contour_min = 0.0
            plotitem.contour_max = 100.0
            plotitem.amr_contour_colors = ['g']  # color on each level
            plotitem.amr_grid_bgcolor = ['#ffeeee', '#eeeeff', '#eeffee']
            plotitem.gridlines_show = 0
            plotitem.gridedges_show = 0
     
    
    def add_transect_label(cd,direction,slice_location):
        if direction == 'x':
            title = "X-Profile y=%s" % slice_location
        else:
            title = "Y-Profile x=%s" % slice_location
        plt.text(0.5,0.05,title,horizontalalignment='center',
                               verticalalignment='center',
                               transform=plt.gca().transAxes,
                               bbox=dict(facecolor='red',alpha=0.1))
    
    def add_transect(plotfigure,direction,location):

        profile_plot = PlotProfile(location,direction) 
    
        plotaxes = plotfigure.new_plotaxes()
        plotaxes.title = "Surface"
        plotaxes.afteraxes = lambda cd: add_transect_label(cd,direction,location) 
        if direction == 'x':
            plotaxes.xlimits = xlimits
        else:
            plotaxes.xlimits = ylimits
        plotaxes.ylimits = [-1.2,surface_limits[1]]
        plotaxes.axescmd = 'subplot(2,1,1)'
    
        plotitem = plotaxes.new_plotitem(plot_type="1d_from_2d_data")
        plotitem.map_2d_to_1d = profile_plot.surface_profile
        plotitem.color = 'b'
        plotitem = plotaxes.new_plotitem(plot_type="1d_from_2d_data")
        plotitem.map_2d_to_1d = profile_plot.bathy_profile
        plotitem.color = 'k'

        plotaxes = plotfigure.new_plotaxes()
        plotaxes.title = "Velocities"
        if direction == 'x':
            plotaxes.xlimits = xlimits
        else:
            plotaxes.xlimits = ylimits
        plotaxes.ylimits = [min(velx_limits[0],vely_limits[0]),
                            max(velx_limits[1],vely_limits[1])]
        plotaxes.axescmd = 'subplot(2,1,2)'

        plotitem = plotaxes.new_plotitem(plot_type="1d_from_2d_data")
        plotitem.map_2d_to_1d = profile_plot.x_velocity_profile
        plotitem.color = 'k'
        plotitem = plotaxes.new_plotitem(plot_type="1d_from_2d_data")
        plotitem.map_2d_to_1d = profile_plot.y_velocity_profile
        plotitem.color = 'r'
        
        
    # ========================================================================
    #  Plot Limits
    # ========================================================================
    #xlimits = [0.0,30.0]
    #ylimits = [0.0,30.0]
    xlimits = [clawdata.xlower,clawdata.xupper]
    ylimits = [clawdata.ylower,clawdata.yupper]
    surface_limits = [-1.0,1.0]
    speed_limits = [0.0,0.3]
    velx_limits = [-speed_limits[1],speed_limits[1]]
    vely_limits = velx_limits
    #vely_limits = [-0.1,0.1]
    tracer_limits = [0.0,1.0]
    vorticity_limits = [-0.1,0.1]

    # ========================================================================
    #  Surface Elevation
    # ========================================================================
    plotfigure = plotdata.new_plotfigure(name="surface",figno=0)
    plotfigure.show = True
    
    plotaxes = plotfigure.new_plotaxes()
    plotaxes.title = "Surface"
    plotaxes.scaled = True
    plotaxes.xlimits = xlimits
    plotaxes.ylimits = ylimits
    plotaxes.afteraxes = pcolor_afteraxes
    
    add_surface_elevation(plotaxes,bounds=surface_limits)
    add_land(plotaxes)
    
    # ========================================================================
    #  Water Speed
    # ========================================================================
    plotfigure = plotdata.new_plotfigure(name="speed",figno=100)
    plotfigure.show = True
    
    plotaxes = plotfigure.new_plotaxes()
    plotaxes.title = "Currents"
    plotaxes.scaled = True
    plotaxes.xlimits = xlimits
    plotaxes.ylimits = ylimits
    plotaxes.afteraxes = pcolor_afteraxes
    
    add_speed(plotaxes,bounds=speed_limits)
    add_land(plotaxes)
    
    # ========================================================================
    #  Water Velocities
    # ========================================================================
    plotfigure = plotdata.new_plotfigure(name="velocities",figno=101)
    plotfigure.show = True
    plotfigure.kwargs = {'figsize':(14,7)}
    
    plotaxes = plotfigure.new_plotaxes()
    plotaxes.title = "X-Velocity"
    plotaxes.scaled = True
    plotaxes.axescmd = 'subplot(1,2,1)'
    plotaxes.xlimits = xlimits
    plotaxes.ylimits = ylimits
    plotaxes.afteraxes = pcolor_afteraxes
    
    add_x_velocity(plotaxes,bounds=velx_limits)
    add_land(plotaxes)

    plotaxes = plotfigure.new_plotaxes()
    plotaxes.title = "Y-Velocity"
    plotaxes.scaled = True
    plotaxes.axescmd = 'subplot(1,2,2)'
    plotaxes.xlimits = xlimits
    plotaxes.ylimits = ylimits
    plotaxes.afteraxes = pcolor_afteraxes

    add_y_velocity(plotaxes,bounds=vely_limits)
    add_land(plotaxes)
     
    # ========================================================================
    #  Tracer
    # ========================================================================
    plotfigure = plotdata.new_plotfigure(name="tracer",figno=200)
    plotfigure.show = True
    
    plotaxes = plotfigure.new_plotaxes()
    plotaxes.title = "Tracer"
    plotaxes.scaled = True
    plotaxes.xlimits = xlimits
    plotaxes.ylimits = ylimits
    plotaxes.afteraxes = pcolor_afteraxes
    
    add_tracer(plotaxes,bounds=tracer_limits)
    add_land(plotaxes)
    
    # ========================================================================
    #  Vorticity
    # ========================================================================
    plotfigure = plotdata.new_plotfigure(name="vorticity",figno=300)
    plotfigure.show = True
    plotaxes = plotfigure.new_plotaxes()
    plotaxes.title = "Vorticity"
    plotaxes.scaled = True
    plotaxes.xlimits = xlimits
    plotaxes.ylimits = ylimits
    plotaxes.afteraxes = pcolor_afteraxes
    
    add_vorticity(plotaxes,bounds=vorticity_limits)
    add_land(plotaxes)
    
    # ========================================================================
    #  Averages
    # ========================================================================
    plotfigure = plotdata.new_plotfigure(name="Test Average",figno=400)
    plotfigure.show = True
    plotaxes = plotfigure.new_plotaxes()
    plotaxes.title = "Test Average"
    plotaxes.scaled = True
    plotaxes.xlimits = xlimits
    plotaxes.ylimits = ylimits
    plotaxes.afteraxes = pcolor_afteraxes
    
    add_average(plotaxes,1,bounds=None)
    add_land(plotaxes)
    
    plotfigure = plotdata.new_plotfigure(name="U eta / H",figno=401)
    plotfigure.kwargs = {'figsize':(14,7)}
    plotfigure.show = True
    plotaxes = plotfigure.new_plotaxes()
    plotaxes.title = "ave(u eta / H)"
    plotaxes.scaled = True
    plotaxes.axescmd = 'subplot(121)'
    plotaxes.xlimits = xlimits
    plotaxes.ylimits = ylimits
    plotaxes.afteraxes = pcolor_afteraxes
    
    add_average(plotaxes,5,bounds=None)
    add_land(plotaxes)
    
    plotaxes = plotfigure.new_plotaxes()
    plotaxes.title = "ave(V eta / H)"
    plotaxes.scaled = True
    plotaxes.axescmd = 'subplot(122)'
    plotaxes.xlimits = xlimits
    plotaxes.ylimits = ylimits
    plotaxes.afteraxes = pcolor_afteraxes
    
    add_average(plotaxes,6,bounds=None)
    add_land(plotaxes)

    plotfigure = plotdata.new_plotfigure(name="uu",figno=402)
    plotfigure.show = True
    plotaxes = plotfigure.new_plotaxes()
    plotaxes.title = "ave(uu)"
    plotaxes.scaled = True
    plotaxes.xlimits = xlimits
    plotaxes.ylimits = ylimits
    plotaxes.afteraxes = pcolor_afteraxes
    
    add_average(plotaxes,2,bounds=None)
    add_land(plotaxes)
    
    plotfigure = plotdata.new_plotfigure(name="Vortex Force",figno=403)
    plotfigure.kwargs = {'figsize':(14,7)}
    plotfigure.show = True
    plotaxes = plotfigure.new_plotaxes()
    plotaxes.title = "Vortex Force ave(v zeta)"
    plotaxes.scaled = True
    plotaxes.axescmd = 'subplot(121)'
    plotaxes.xlimits = xlimits
    plotaxes.ylimits = ylimits
    plotaxes.afteraxes = pcolor_afteraxes
    
    add_average(plotaxes,3,bounds=None)
    add_land(plotaxes)
    
    plotaxes = plotfigure.new_plotaxes()
    plotaxes.title = "Vortex Force ave(-u zeta)"
    plotaxes.scaled = False
    plotaxes.axescmd = 'subplot(122)'
    plotaxes.xlimits = xlimits
    plotaxes.ylimits = ylimits
    plotaxes.afteraxes = pcolor_afteraxes
    
    add_average(plotaxes,4,bounds=None)
    add_land(plotaxes)
    
    # ========================================================================
    #  Bathymetry
    # ========================================================================
    plotfigure = plotdata.new_plotfigure(name="bathy",figno=500)
    plotfigure.show = False
    plotaxes = plotfigure.new_plotaxes()
    plotaxes.title = "Bathymetry"
    plotaxes.scaled = True
    plotaxes.xlimits = xlimits
    plotaxes.ylimits = ylimits
    plotaxes.afteraxes = pcolor_afteraxes
    
    plotitem = plotaxes.new_plotitem(plot_type='2d_pcolor')
    plotitem.show = True
    plotitem.plot_var = b
    plotitem.pcolor_cmap = plt.get_cmap('gist_earth')
    plotitem.pcolor_cmin = -1.0
    plotitem.pcolor_cmax = 1.0
    plotitem.add_colorbar = True

    # ========================================================================
    # Figure for grids alone
    # ========================================================================
    plotfigure = plotdata.new_plotfigure(name='grids', figno=900)
    plotfigure.show = False

    # Set up for axes in this figure:
    plotaxes = plotfigure.new_plotaxes()
    plotaxes.xlimits = xlimits
    plotaxes.ylimits = ylimits
    plotaxes.title = 'Grids'
    plotaxes.scaled = True

    # Set up for item on these axes:
    plotitem = plotaxes.new_plotitem(plot_type='2d_grid')
    plotitem.amr_grid_bgcolor = ['#ffeeee', '#eeeeff', '#eeffee']
    plotitem.amr_gridlines_show = [1,1,0]   
    plotitem.amr_gridedges_show = [1]     

    # ======================================================================== 
    #  Profiles
    # ========================================================================  
    # Transect in x-direction, constant y
    plotfigure = plotdata.new_plotfigure(name='mid_profile_x',figno=1000)
    plotfigure.show = True
    slice_location = (clawdata.yupper - clawdata.ylower) / 2.0
    add_transect(plotfigure,'x',slice_location)

    # Transect in y-direction, constant x
    plotfigure = plotdata.new_plotfigure(name="mid_profile_y",figno=1001)
    plotfigure.show = True
    slice_location = (clawdata.xupper - clawdata.xlower) / 2.0
    add_transect(plotfigure,'y',slice_location)
    
    # Transect along forced boundary
    plotfigure = plotdata.new_plotfigure(name="bc_profile",figno=1002)
    plotfigure.show = True
    add_transect(plotfigure,'y',clawdata.xlower)
    
    # Transect along wall 
    plotfigure = plotdata.new_plotfigure(name="wall_profile",figno=1003)
    plotfigure.show = True
    add_transect(plotfigure,'y',clawdata.xupper)
     
    # Parameters used only when creating html and/or latex hardcopy
    # e.g., via pyclaw.plotters.frametools.printframes:

    plotdata.printfigs = True                # print figures
    plotdata.print_format = 'png'            # file format
    plotdata.print_framenos = 'all'          # list of frames to print
    plotdata.print_gaugenos = []             # list of gauges to print
    plotdata.print_fignos = 'all'            # list of figures to print
    plotdata.html = True                     # create html files of plots?
    plotdata.latex = True                    # create latex file of plots?
    plotdata.latex_figsperline = 2           # layout of plots
    plotdata.latex_framesperline = 1         # layout of plots
    plotdata.latex_makepdf = False           # also run pdflatex?

    return plotdata

    
