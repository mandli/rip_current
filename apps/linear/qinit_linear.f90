subroutine qinit(maxmx,maxmy,meqn,mbc,mx,my,xlower,ylower,dx,dy,q,maux,aux)

    use wave_tank_module

    implicit none
    
    ! Input/Output Arguments
    integer, intent(in) :: maxmx,maxmy,meqn,mbc,mx,my,maux
    double precision, intent(in) :: xlower,ylower,dx,dy

    double precision, intent(inout) :: q(1-mbc:maxmx+mbc,1-mbc:maxmy+mbc,meqn)
    double precision, intent(inout) :: aux(1-mbc:maxmx+mbc,1-mbc:maxmy+mbc,maux)

    ! Local storage
    integer :: i,j
    double precision :: x,y,x0,y0
    double precision, parameter :: SEA_LEVEL = 0.d0
!     double precision, parameter :: MAX_HEIGHT = 0.25d0
    
    x0 = 0.5d0 * mx * dx
    y0 = 0.5d0 * my * dy
    
    do j=1,my
        y = ylower + (j-0.5d0) * dy
        do i=1,mx
            x = xlower + (i-0.5d0) * dx
            q(i,j,1) = SEA_LEVEL ! Perturbation from sea_level
            aux(i,j,2) = max(0.d0,sea_level - aux(i,j,1)) ! Initial depth

            q(i,j,2:3) = 0.d0 ! Perturbation from u/v momentum
            
            if (tracer(1) <= x .and. x <= tracer(2) .and. (aux(i,j,2) > 0.d0)) then
                q(i,j,4) = 1.d0
            else
                q(i,j,4) = 0.d0
            endif
!             q(i,j,1) = q(i,j,1) + MAX_HEIGHT * exp( -((x-x0)**2 + (y-y0)**2) )
        enddo
    enddo

end subroutine qinit
