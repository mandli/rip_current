subroutine src2(maxmx,maxmy,meqn,mbc,mx,my,xlower,ylower,dx,dy,q,maux,aux,t,dt) 

    use wave_tank_module 
    use geoclaw_module, only: grav

    implicit none

    ! Input/Output Arguments
    integer, intent(in) :: maxmx,maxmy,meqn,mbc,mx,my,maux
    double precision, intent(in) :: xlower,ylower,dx,dy,t,dt

    double precision, intent(inout) :: q(1-mbc:maxmx+mbc, 1-mbc:maxmy+mbc, meqn)
    double precision, intent(inout) :: aux(1-mbc:maxmx+mbc, 1-mbc:maxmy+mbc, maux)

    ! Local storage
    integer :: i,j
    double precision :: h,hu,hv,gamma,dgamma

    ! Algorithm parameters
    double precision, parameter :: tolerance = 1.d-30

    ! Friction source term
    if (friction) then
        do i=1,mx
            do j=1,my
                h = q(i,j,1) + aux(i,j,2)
                hu = q(i,j,2)
                hv = q(i,j,3)

                if (h < tolerance) then
                    q(i,j,2:3) = 0.d0
                else
                    gamma = sqrt(hu**2 + hv**2) * (grav*coeff**2) / h**(7/3)
                    dgamma = 1.d0 + dt*gamma
                    q(i,j,2:3) = q(i,j,2:3) / dgamma 
                endif
            enddo
        enddo
    endif

end subroutine src2
