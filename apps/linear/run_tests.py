#!/usr/bin/env python
# encoding utf-8
r"""
Runs tests for wave tank rip current numerical experiments

:Authors:
    Kyle T. Mandli (2011-09-27) Initial Version
"""

import os
import sys

import numpy as np

import test_runs

class WaveTankBaseTest(test_runs.Test):

    def __init__(self,friction=0.025,xlower=0.0,xupper=10.0*np.pi,
            ylower=0.0,yupper=10.0*np.pi,mx=150,my=150,
            depth=-1.0,knee=25.0,angle=0.0):
        super(WaveTankBaseTest,self).__init__()

        import setrun

        # self.type = "wave_tank" Test does not have type, yet
        self.name = "wave_tank"
        self.setplot = "setplot"

        self.run_data = setrun.setrun()
        
        # GeoClaw test parameters
        self.run_data.clawdata.xlower = xlower
        self.run_data.clawdata.xupper = xupper
        self.run_data.clawdata.ylower = ylower
        self.run_data.clawdata.yupper = yupper
        self.run_data.clawdata.mx = mx
        self.run_data.clawdata.my = my

        # Wave tank test parameters
        self.wave_tank = setrun.set_wave_tank_data(self.run_data.clawdata)
        self.wave_tank.depth = depth
        self.wave_tank.knee = knee
        self.wave_tank.angle = angle
        
        self.wave_tank.amplitude = [0.2,0.2]
        self.wave_tank.wave_length = [10.0*np.pi,-10.0*np.pi]
        self.wave_tank.frequency = [-2./np.pi,-2./np.pi]
        self.wave_tank.phi_t = [0.0,0.0]
        self.wave_tank.phi_x = [30.0,0.0]
        
        if angle == 0.0:
            self.prefix = "bath"
            shore = knee - depth / np.tan(angle)
        else:
            self.prefix = "beach"
            shore = xupper
        self.wave_tank.tracer = [shore - 3.0, 40.0]

        periods = int(np.ceil((yupper-ylower) / np.abs(self.wave_tank.wave_length[0])))
        self.prefix = "_".join((self.prefix,"T%s" % periods))
        if friction == 0.0:
            self.prefix = "_".join((self.prefix,"fF"))
        else:
            self.prefix = "_".join((self.prefix,"fT"))

    def __str__(self):
        output = super(WaveTankBaseTest,self).__str__()
        output += "\nRundata:     \n%s" % self.run_data
        output += "-"*30
        output += "\nWave Tank:   \n%s" % self.wave_tank
        #output += "-"*30
        #output += "\nBathymetry:  \n%s" % self.bathy_profile
        return output

    def write_data_objects(self):
        self.run_data.write()
        self.wave_tank.write()
   
        # Create bathymetry file

tests = []
# Base tests
#test = WaveBaseTest(xlower=0.0,xupper=30.0,depth=-1.0,knee=30.0,angle=0.0)
#test.prefix = "3a"
#tests.append(test)
#test = WaveBaseTest(xlower=0.0,xupper=25.0,depth=-1.0,knee=30.0,angle=0.0)
#test.prefix = "3b"
#tests.append(test)
#test = WaveBaseTest(xlower=0.0,xupper=30.0,depth=-2.5,knee=0.0,angle=np.tan(0.1))
#test.prefix = "3c"
#tests.append(test)
#test = WaveBaseTest(xlower=0.0,xupper=30.0,depth=-1.5,knee=30.0,angle=np.tan(1.8/30.0))
#test.prefix = "3d"
#tests.append(test)

# Beach tests
tests.append(WaveTankBaseTest(depth=-1.0,knee=15.0,angle=np.arctan(0.1)))

# Bath tub tests
# Basic test
tests.append(WaveTankBaseTest()) 
# Periodicity test
tests.append(WaveTankBaseTest(my=150*3,ylower=-10.0*np.pi,yupper=20*np.pi)) # Testing periodicity
# Frictionless bottom
tests.append(WaveTankBaseTest(xupper=10.0*np.pi,yupper=10.0*np.pi,friction=0.0))

if __name__ == "__main__":
    if len(sys.argv) > 1:
        if sys.argv[1].lower() == 'all':
            tests_to_be_run = tests
        else:
            tests_to_be_run = []
            for test in sys.argv[1:]:
                tests_to_be_run.append(tests[int(test)])
            
        test_runs.run_tests(tests_to_be_run,parallel=True)

    else:
        test_runs.print_tests(tests)   
        
