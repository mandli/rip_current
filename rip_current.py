#!/usr/bin/env python
# encoding: utf-8

r"""Solve 2d SWE setup with rip current setup

       Periodic
+---------------------+
|                     |
|                     |
|                     |
| Forced              | Wall
|                     |
|                     |
|                     |
+---------------------+
       Periodic
"""

import os

import numpy

import clawpack.pyclaw.util as util
import clawpack.geoclaw.topotools as tt

def create_incoming_wave_function(theta, width, A, g=9.81, H=2.0):
    r"""The sea-surface elevation and momenta of two plane-waves

    Plane-waves are at an angle to each other

    """

    c = numpy.sqrt(g * H)
    L = 2.0 * width * numpy.sin(theta)

    def incoming_wave(x, y, t):
        eta = numpy.zeros((2, y.shape[0]))
        eta[0, :] = A[0] * numpy.sin(2.0 * numpy.pi * (x * numpy.cos(theta) 
                                                     + y * numpy.sin(theta) 
                                                     - c * t) / L)
        eta[1, :] = A[1] * numpy.sin(2.0 * numpy.pi * (x * numpy.cos(theta) 
                                                     - y * numpy.sin(theta)
                                                     - c * t) / L)

        h = H + eta[0, :] + eta[1, :]
        hu = h * numpy.sqrt(g / H) * (eta[0, :] + eta[1, :]) * numpy.cos(theta)
        hv = h * numpy.sqrt(g / H) * (eta[0, :] - eta[1, :]) * numpy.sin(theta) 

        return eta[0, :] + eta[1, :], hu, hv

    return incoming_wave



def create_plane_wave_forcing(theta, width, A, g=9.81, H=2.0):
    r"""Create forcing functions given input parameters

    returns a function of the form f(y, t) for the fields eta, u, and v
    respectively

    """

    c = numpy.sqrt(g * H)
    L = 2.0 * width * numpy.sin(theta)
    wave_length = 2.0 * width * numpy.sin(theta)

    def eta(x, y, t):
        eta = numpy.zeros(y.shape)
        eta += A * numpy.sin(2.0 * numpy.pi * (x * numpy.cos(theta) + y * numpy.sin(theta) - c * t) / wavelength)
        eta += A * numpy.sin(2.0 * numpy.pi * (x * numpy.cos(theta) - y * numpy.sin(theta) - c * t) / wavelength)
        return eta

    def u(x, y, t):
        u = numpy.zeros(y.shape)
        u = numpy.sqrt(g / H) * (eta0 + eta1) * numpy.cos(theta)

        return u

    def v(x, y, t):
        v = numpy.zeros(y.shape)
        v += A[0] * numpy.sin(2.0 * numpy.pi * (  y * numpy.sin(theta) - c * t) / L)
        v -= A[1] * numpy.sin(2.0 * numpy.pi * (- y * numpy.sin(theta) - c * t) / L)
        v *= numpy.sqrt(g / H) * numpy.sin(theta)

        return v


    return eta, u, v


def create_harmonic_wave_forcing(A, wave_length, frequency, phi_t, phi_y):

    # In these BCs u is assumed given and the other two fields are calculated 
    # via the linear relations

    def eta(y, t):
        eta = numpy.zeros(y.shape)
        for (m, f) in enumerate(frequency):
            eta -= 2.0 * numpy.pi * f * A[m] * numpy.cos(2.0 * numpy.pi * 
                        ((y - phi_y[m]) / wave_length[m] - f * (t - phi_t[m])))
        return eta

    def u(y, t):
        u = numpy.zeros(y.shape)
        for (m, f) in enumerate(frequency):
            u += A[m] * numpy.sin(
                                2.0 * numpy.pi * (y - phi_y[m]) / wave_length[m] 
                                - f * (t - phi_t[m]))
        return u

    def v(y, t):
        v = numpy.zeros(y.shape)
        return v        

    return eta, u, v


# def forced_boundary(state, dim, t, qbc, aux_bc, num_ghost, 
#                                                       eta_star, u_star, v_star):
#     r"""Boundary forcing functions, fills in qbc

#     Note that the function arguments eta_star, u_star, and v_star should all be
#     replaced in the claling function (via a lambda for instance)
#     """

#     g = state.problem_data['g']
#     X, Y = state.grid.p_centers
#     y = Y[0, :]
#     x = X[:, 0]
#     dx = state.grid.dimensions[0].delta

#     method = 'last_resort'

#     if method == "randy":
#         tau = dx / (2.0 * numpy.sqrt(g * state.q[0, 0, :]))
#         d_eta = eta_star(y, t + tau) - eta_star(y, t - tau)
#         h1 = state.q[0, 0, :]
#         c_bar = numpy.sqrt(g * (h1 - 0.5 * d_eta))
#         mu = state.q[1, 0, :] - c_bar * d_eta
#         d_v = v_star(y, t + tau) - v_star(y, t - tau)
#         v = state.q[3, 0, :] / state.q[0, 0, :]
#         nu = (h1 - d_eta) * (v - d_v)
#         h = h1 - d_eta
#         phi = state.q[3, 0, :]
#     elif method == 'direct':
#         H = state.aux[0, 0, 1]
#         eta = eta_star(y, t)
#         h = state.q[0, 0, :] - eta
#         mu = -g * H * state.q[0, 0, :] - eta
#         nu = -g * H * 0.5 * (eta_star(y[1:], t) - eta_star(y[:-1], t))

#         phi = state.q[3, 0, :]

#     elif method == 'new':
#         c_bar_p = numpy.sqrt(g * 0.5 * (state.q[0, 0, :] + state.q[1, 0, :]))
#         deta = state.q[0, 1, :] + state.aux[0, 1, :] - (state.q[0, 0, :] + state.aux[0, 0, :])
#         dmu = state.q[1, 1, :] - state.q[1, 0, :]
#         alpha_1 = (c_bar_p * deta - dmu) / (2.0 * c_bar_p)

#         alpha_2 = state.q[0, 0, :] + state.aux[0, 0, :] - eta_star(y, t)
#         h = eta_star(y, t) - alpha_1 - state.aux[0, 0, :]
#         cbar = numpy.sqrt(g * 0.5 * (h + state.q[0, 0, :]))
#         mu = state.q[1, 0, :] - alpha_2 * cbar + alpha_1 * cbar

#         nu = state.q[2, 0, :]
#         phi = state.q[3, 0, :]

#     elif method == 'last_resort':
#         h = -eta_star(y, t) / g - state.aux[0, 0, :]
#         mu = u_star(y, t) * h
#         nu = state.q[2, 0, :]
#         phi = state.q[3, 0, :]

#     elif method == "new_2":
#         eta1star, hu1star, hv1star = wave(x[0], y, time)
#         h1 = state.q[0, 0, :]
#         hu1 = state.q[1, 0, :]
#         hv1 = state.q[2, 0, :]

#         wave()


def forced_boundary(state, dim, t, qbc, aux_bc, num_ghost, incoming_wave):
    r""""""

    g = state.problem_data['g']
    X, Y = state.grid.p_centers
    y = Y[0, :]
    x = X[:, 0]
    dx = state.grid.dimensions[0].delta

    eta_star = numpy.empty((2, y.shape[0]))
    hu_star = numpy.empty((2, y.shape[0]))
    hv_star = numpy.empty((2, y.shape[0]))
    eta_star[0, :], hu_star[0, :], hv_star[0, :] = incoming_wave(x[0], y, t)
    
    for n in xrange(1, num_ghost + 1):
        eta_star[1, :], hu_star[1, :], hv_star[1, :] = incoming_wave(x[0] - n * dx, y, t)

        qbc[0, num_ghost - n, num_ghost:num_ghost + state.grid.num_cells[1]] = \
                              state.q[0, 0, :] + eta_star[0, :] - eta_star[1, :]
        qbc[1, num_ghost - n, num_ghost:num_ghost + state.grid.num_cells[1]] = \
                              state.q[1, 0, :] + hu_star[0, :] - hu_star[1, :]
        qbc[2, num_ghost - n, num_ghost:num_ghost + state.grid.num_cells[1]] = \
                              state.q[2, 0, :] + hv_star[0, :] - hv_star[1, :]
    qbc[3, :num_ghost, num_ghost:num_ghost + state.grid.num_cells[1]] = \
                              state.q[3, 0, :]


def friction_source(solver, state, dt, manning=0.025, TOLERANCE=1e-30):
    g = state.problem_data['g']
    dry_tolerance = state.problem_data['drytol']
    
    if manning > TOLERANCE:
        hu = numpy.where(state.q[0,:,:] > dry_tolerance, state.q[1,:,:] / state.q[0,:,:], numpy.zeros(state.q[1,:,:].shape))
        hv = numpy.where(state.q[0,:,:] > dry_tolerance, state.q[2,:,:] / state.q[0,:,:], numpy.zeros(state.q[2,:,:].shape))

        wet_indices = numpy.nonzero(state.q[0,:,:] > dry_tolerance)
        gamma  = g * manning**2 / state.q[0, wet_indices]**(7.0 / 3.0)  \
                        * numpy.sqrt(state.q[1, wet_indices]**2         \
                                   + state.q[2, wet_indices]**2)
        dgamma = 1.0 + dt * gamma
        state.q[2:4, wet_indices] = state.q[2:4, wet_indices] / dgamma


def setup(use_petsc=True, outdir="./_output", solver_type="classic",
          nonlinear=True):

    if use_petsc:
        import clawpack.petclaw as pyclaw
    else:
        from clawpack import pyclaw

    # Setup solver
    if solver_type == 'classic':
        solver = pyclaw.ClawSolver2D()
        solver.dimensional_split = False
        # solver.dimensional_split = True
        # solver.order = 1
        # solver.cfl_max = 0.5
        # solver.cfl_desired = 0.4
        solver.cfl_max = 1.0
        solver.cfl_desired = 0.9
    elif solver_type == 'sharpclaw':
        solver = pyclaw.ClawSolver2D()

    if nonlinear:
        import rp_nonlinear_swe
        solver.rp = rp_nonlinear_swe
    else:
        import rp_linear_swe
        solver.rp = rp_linear_swe

    solver.num_eqn = 4
    solver.num_waves = 4
    solver.limiters = pyclaw.limiters.tvd.minmod

    solver.fwave = True
    solver.source_split = 1
    solver.step_source =   \
        lambda solver, state, dt:friction_source(solver, state, dt, manning=0.0)

    # Physical parameters
    g = 9.8
    sea_level = 0.0

    # Domain
    delta = 0.5
    x_length = 150.0
    # x_length = 30.0
    y_length = 10.0 * numpy.pi # * 3.0

    x = pyclaw.Dimension(0.0, x_length, numpy.floor(x_length / delta), name='x')
    y = pyclaw.Dimension(0.0, y_length, numpy.floor(y_length / delta), name='y')
    domain = pyclaw.Domain([x, y])

    # Set up initial state
    num_eqn = 4
    num_aux = 1
    state = pyclaw.State(domain, num_eqn, num_aux)

    # Bathymetry
    flat_topo = [(x.lower, -1.0), (x.upper, -1.0)]
    beach_slope = 0.01
    beach_knee = 30.0
    shore_depth = -0.1
    outer_depth = (beach_knee - x.upper) * beach_slope + shore_depth
    shoaling_topo = [(x.lower, outer_depth), (beach_knee, outer_depth),
                     (x.upper, shore_depth)]

    topo_func = tt.create_topo_func(shoaling_topo, verbose=False)
    # topo_func = tt.create_topo_func(flat_topo, verbose=False)
    state.aux[0, :, :] = topo_func(*state.p_centers)

    # Initial condition
    state.q[0, :, :] = numpy.maximum(0.0, sea_level - state.aux[0, :, :])
    state.q[1, :, :] = 0.0
    state.q[2, :, :] = 0.0

    # tracer_range = [22.0, 40.0]
    # tracer_range = [22.0, 28.0]
    tracer_range = [135.0, 145.0]
    if tracer_range is not None:
        X, Y = state.grid.p_centers
        state.q[3,:,:] = ( tracer_range[0] <= X) * (X <= tracer_range[1] )  \
                         * numpy.ones(X.shape)

    state.problem_data['g'] = g
    state.problem_data['drytol'] = 1e-3

    # Forcing parameters
    amplitude = [0.01, 0.01]
    # amplitude = [0.05, 0.05]
    # wave_length = [10.0 * numpy.pi]
    # frequency = [-2.0 / numpy.pi]
    # phi_t = [0.0]
    # phi_y = [30.0]
    # amplitude = [0.1, 0.1]
    # amplitude = [0.01, 0.01]
    wave_length = [10.0 * numpy.pi, -10.0 * numpy.pi]
    frequency = [-2.0 / numpy.pi, -2.0 / numpy.pi]
    phi_t = [0.0,0.0]
    phi_y = [30.0,0.0]

    # theta, width, amplitudes, g=9.81, H=2.0
    # eta_bc, u_bc, v_bc = create_plane_wave_forcing(numpy.pi / 8.0, 
    #                                                y.upper - y.lower, amplitude, 
    #                                                g=g, H=-outer_depth)
    # eta_bc, u_bc, v_bc = create_harmonic_wave_forcing(amplitude, wave_length, 
    #                                                   frequency, phi_t, phi_y)
    # solver.user_bc_lower = lambda state, dim, t, qbc, aux_bc, num_ghost: \
    #                       forced_boundary(state, dim, t, qbc, aux_bc, num_ghost, 
    #                                                          eta_bc, u_bc, v_bc)
    incoming_wave = create_incoming_wave_function(numpy.pi / 8.0, 
                                                   y.upper - y.lower, amplitude, 
                                                   g=g, H=-outer_depth)
    solver.user_bc_lower = lambda state, dim, t, qbc, aux_bc, num_ghost:   \
                          forced_boundary(state, dim, t, qbc, aux_bc, num_ghost, 
                                          incoming_wave)

    solver.bc_lower[0] = 0
    solver.bc_upper[0] = pyclaw.BC.wall
    solver.bc_lower[1] = pyclaw.BC.periodic
    solver.bc_upper[1] = pyclaw.BC.periodic

    solver.aux_bc_lower[0] = pyclaw.BC.extrap
    solver.aux_bc_upper[0] = pyclaw.BC.wall
    solver.aux_bc_lower[1] = pyclaw.BC.periodic
    solver.aux_bc_upper[1] = pyclaw.BC.periodic
    # solver.aux_bc_lower[1] = pyclaw.BC.wall
    # solver.aux_bc_upper[1] = pyclaw.BC.wall

    # Setup controller
    claw = pyclaw.Controller()
    claw.solution = pyclaw.Solution(state, domain)
    claw.solver = solver
    claw.outdir = outdir
    claw.setplot = setplot
    claw.keep_copy = False

    # Output control
    claw.output_format = 'petsc'
    claw.write_aux_init = True
    claw.output_style = 1
    tfinal = 800.0
    dt = numpy.pi / 2.0
    claw.num_output_times = int(tfinal / dt) + 1
    claw.tfinal = dt * claw.num_output_times

    # Debugging
    # claw.output_style = 3
    # claw.num_output_times = 100

    return claw


if __name__=="__main__":
    # Compile Riemann solvers if needed
    nonlinear_src = ["rpn2_nonlinear_swe.f90", 
                     "rpt2_nonlinear_swe.f90", 
                os.path.expandvars("$CLAW/riemann/src/geoclaw_riemann_utils.f")]
    linear_src = ["rpn2_linear_swe.f90", "rpt2_linear_swe.f90",
                os.path.expandvars("$CLAW/riemann/src/geoclaw_riemann_utils.f")]
    FFLAGS = "-O3 -funroll-loops -finline-functions"
    util.compile_library(nonlinear_src, 'rp_nonlinear_swe', FFLAGS=FFLAGS)
    # util.compile_library(linear_src, 'rp_linear_swe', FFLAGS=FFLAGS)

    # Run application
    from clawpack.pyclaw.util import run_app_from_main
    import setplot
    output = run_app_from_main(setup, setplot.setplot)